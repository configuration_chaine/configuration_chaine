#!/usr/bin/perl

package configuration;

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell

=head1 NAME

configuration - génère un fichier de configuration avec les chaînes souhaitées 
                                     et introduit les données dans mythconverg.

=head1 SYNOPSIS

 To use: 
   'configuration' est un sous-programme de 'configuration_chaine.pl' 

=head1 DESCRIPTION

Ce script permet de 
  - génèrer un fichier comprenant pour toutes les chaînes 
        un numéro, 
	un nom, 
	un identifiant xmltvid, 
	une adresse de téléchargement de l'icone,
        le numéro sid pour les chaînes du satellite

Il est compatible avec les differents mode de réception de télévision disponible en France ou en Europe
(Hertzien, Cable/satellite, Canal+ Sat, ...) et avec les différents récupérateurs de programmes disponible en France.
Ce script parcourt le net pour fournit les informations les plus à jour possible.
Parmi les sites utilisés, se trouve lyngsat.com, telerama.fr, telepoche.fr.......
Il stocke les informations sous forme de fichier qui est par defaut enregistre sous ~/.xmltv/sat_grab.conf.
Si les informations ne sont pas disponibles sur le net, elles sont mises à diposition sur le serveur de mytharch sous forme de fichier.


=head1 AUTHOR
Gilles  C
gillessoixantequatorze@orange.fr

Rapporte les bugs sur <http://mythtv-fr.org/bugtracker/index.php?project=5>

=cut

# TODO / FIXME     prevoir une fenetre de progression 

use warnings;
use strict;
use Getopt::Long;
use IO::File;
use LWP;
use File::Temp;
use LWP::Simple;
use POSIX;
use utf8; 
use Unicode::Normalize; 
use Tk;
use Tk::ProgressBar;
use Storable;
use Storable qw(nstore retrieve); 


#***************************************************************************
# Main declarations
#***************************************************************************

my $GRID_FOR_CHANNEL = 'http://www.lyngsat.com/packages/';
my $ROOT_URL  = '';
my $CHECK_CHANNEL_URL = '';

my $VERSION   = "300111";


# liste des sources supportees
# Grid id defined by the website according to channel types (needed to build the URL)
my %GridType = (  "ORANGE Sat sur Astra" => "orangeastra",
		"ORANGE Sat sur Hotbird"        => "orangehb",
		"ORANGE Sat sur AB3"  => "orangeab3",
		"CANAL Sat sur Astra"  => "canalsatfr",
		"BIS sur AB3"    => "bisab3",
		"FRANSAT sur AB3" => "fransat", 
		"CANAL Sat Caraibes"      => "canalsatcaraibes" ,
		"TNT"  => "tnt_fr" ,
		"TNT Etrangere" => "tntetrangere" );
 
# liste des grabbeurs		  
my %XmlGrab = ( "tv_grab_fr (telepoche)" => "tv_grab_fr",
			"tv_grab_fr_telerama" => "telerama",
			"tv_grab_fr_iphone" => "iphone",
			"KaZeer" => "kazer",
			"mc2xml" => "mc2xml");

#***************************************************************************
# Global variables allocation according to options
#***************************************************************************
my %name_map;
my %num_map;
my %xmlid_map;
#my (%xmlid_map1 , %xmlid_map2 , %xmlid_map3 , %xmlid_map4 , %xmlid_map5 , %xmlid_map6) ;
my $gridid =();
my %configuration =();

#***************************************************************************
# File that stores which channels to download.
#***************************************************************************
	my $home = $ENV{HOME};
	$home = '.' if not defined $home;
	my $conf_dir = "$home/.mythtv";
	(-d $conf_dir) or mkdir($conf_dir, 0777)
	or die "cannot mkdir $conf_dir: $!";

	my $config_file ="$conf_dir/config_file";
	
#***************************************************************************
# Sub sections
#***************************************************************************

sub get_channels( $ );
#sub Nom ();
sub channel_tnt ();
sub get_xmlid ();
sub get_xmlid_iphone () ;
sub get_channels_tnt ( $ );
sub get_channels_etrang( $);
sub ask_boolean($);
sub say($);
sub num_canalsat( ) ;




#***************************************************************************
# MAIN CODE
#***************************************************************************
sub creation_fichier_conf (){

		# recuperer la liste des chaines disponibles par type de diffusion
		#Get a list of available channels, according to the grid type
			my @gts = sort keys %GridType;
			my @gtnames = map { $GridType{$_} } @gts;
			my ($gt, $gtname) ;
				
				foreach my $i (0 .. $#gts) {
					($gt, $gtname) = ($gts[$i], $gtnames[$i]);
					my %channels  = ( );
					get_channels_tnt ( $gtname ) if ( $gtname eq 'tnt_fr');
					get_channels_etrang ( $gtname ) if ( $gtname eq 'tntetrangere');
					get_channels( $gtname ) if (! ( $gtname  eq 'tnt_fr') and ! ( $gtname  eq 'tntetrangere'));
					}

		# recuperer les numeros de canalsat
			num_canalsat( ) ;
		

		#recuperer tous les xmlid de tous les grabbeurs
			get_xmlid_iphone(  ) ;
			get_xmlid( )  ;
			
			#acceder a la liste de chaines
				for my $nom_source (sort keys %configuration) {
					my @list_bouquets = sort keys %{$configuration{$nom_source}}; 
						for my $nom_bouquet (@list_bouquets) {
							my @list_options = sort keys %{$configuration{$nom_source}{$nom_bouquet}};
								for my $nom_option (@list_options) {
									my @list_chaines = sort keys %{$configuration{$nom_source}{$nom_bouquet}{$nom_option}};
									for my $nom_chaine (@list_chaines) {
										#introduire les xmlid's de tous les grabbeurs
										foreach my $i (keys %XmlGrab){
											my $k = $XmlGrab{$i};
											$k = "telepoche" if ($k eq "tv_grab_fr"); 
											$k = "xmltvid_$k";
											$configuration{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{$k} = "";
											$configuration{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{$k} = $xmlid_map{$nom_chaine}{$k} if  $xmlid_map{$nom_chaine}{$k};
										}
										$configuration{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_canalsat} = "";
										$configuration{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_canalsat} = $num_map{$nom_chaine}{canalsat} if  $num_map{$nom_chaine}{canalsat};
									}
								}						
						}	
					}

	nstore \%configuration, "$config_file"|| die "can't store to $config_file\n";

			say( 
	"SORTIE DU PROGRAMME 

	La configuration est maintenant terminée.
	Les données sont enregistrées sous 
	$config_file 
		    

	AU REVOIR." );
}


#***************************************************************************
# Subroutine  - specific functions for grabbing information
#***************************************************************************

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Get the channel from a grid id
sub get_channels( $ ) {
	$gridid = shift;

	# recuperation des noms, sid et icones pour le sat
	my $url = $GRID_FOR_CHANNEL.$gridid.'_sid.html';
	my %name_map = get_name_icone_sid ( $url ,$gridid) ;
	foreach my $name(keys %name_map ){
#print "$name\t $name_map{name}\n";
		$configuration{Satellite}{$gridid}{base}{ $name} = {'source' => 'Satellite'  , 'bouquet'=> $name_map{$name}{bouquet} , 
		'option' => 'Base' ,'name' => $name, 'icone' => $name_map{$name}{icone} ,'numero' => "",
		'sid' => $name_map{$name}{sid}, 'cocher' => '0', 'in_mythtv' =>'0', 'modified_in_mythtv' => '0'} if $name ;
	}
		
  return ;
}

#----------------------------------------------------------------------------------------------------------------------------
# genere les info pour la tnt  partir du fichier de mytharch et les icones et noms  de lyngsat
sub get_channels_tnt ( $ ) {
	$gridid = shift;

	my $content = get("http://download.tuxfamily.org/mythtvarch/configuration/liste_chaines_tnt.txt");                # mettre le contenu dans une variable
	my @list_tnt = split('\n', $content);                     # éclater le fichier en champ
	#recuperateur des icones pour la tnt
	my %icone_map = get_name_icone_sid (  "http://www.lyngsat.com/packages/canalsatfr_sid.html" ,$gridid) ;
	foreach my $ch_tnt (@list_tnt) 
	{
		if ($ch_tnt =~ m/^\d/){
			my @liste_tnt = split('\|', $ch_tnt);                     # éclater le fichier en champ
			my $channel_tnt= $liste_tnt[1];
			$channel_tnt = CleanNom ($channel_tnt);
			my $icone = "";
			$icone = $icone_map{$channel_tnt}{icone} if $icone_map{$channel_tnt};            #prendre de preference l'icone du site lyngsat si elle existe
			$icone = $liste_tnt[2] if ($liste_tnt[2] and !$icone);
			$configuration{Hertzien}{$gridid}{$liste_tnt[3]}{ $channel_tnt} = {'source' => "Hertzien"  , 'bouquet'=> $gridid  , 'option' => $liste_tnt[3], 'sid' => "",
				'name' => $channel_tnt , 'icone' => $icone, 'numero_tnt' => $liste_tnt[0]  , 'cocher' => '0', 'in_mythtv' =>'0', 'modified_in_mythtv' => '0'} if $channel_tnt;
		}
	}
	return  ; 
}

#----------------------------------------------------------------------------------------------------------------------------
# genere les info pour la tnt etrangere partir du fichier de mytharch et les icones et noms lyngsat
sub get_channels_etrang ( $ ) {
	 $gridid = shift;
	my $content = get("http://download.tuxfamily.org/mythtvarch/configuration/liste_chaines_etrangeres.txt");                # mettre le contenu dans une variable
	my @list_etr = split('\n', $content);                     # éclater le fichier en champ
	my %map_icone = get_name_icone_sid (  "http://www.lyngsat.com/packages/canalsatfr_sid.html" , $gridid) ;
	foreach my $ch_tnt_etr (@list_etr) 
	{
		if ($ch_tnt_etr =~ m/^\d/){
			my @liste_etr = split('\|', $ch_tnt_etr);                     # éclater le fichier en champ
			my $channel_etr= $liste_etr[1];
			$channel_etr = CleanNom ($channel_etr) ;
			my $icone = "";
			$icone = $map_icone{$channel_etr}{icone} if ($map_icone{$channel_etr}and $channel_etr) ;
			$icone = $liste_etr[2] if ($liste_etr[2] and !$icone);
			$configuration{Hertzien}{$gridid}{$liste_etr[3]}{ $channel_etr} = {'source' => "Hertzien"  , 'bouquet'=> $gridid , 'option' => $liste_etr[3], 'sid' => "",
				'name' => $channel_etr , 'icone' => $icone, 'numero_tnt' => $liste_etr[0] , 'cocher' => '0', 'in_mythtv' =>'0', 'modified_in_mythtv' => '0'} if $channel_etr ;
		}
	}
	return ; 

}
#--------------------------------------------------------------------------------------------------------------------------------
# recuperation des xmlid de telerama
sub get_xmlid_iphone () {
	
	# telecharge les xmltvid du site de telerama
	my $ua = LWP::UserAgent->new (	agent =>"Telerama/1.2 CFNetwork/459 Darwin/10.0.0d3");
	my $req =HTTP::Request->new( GET =>"http://guidetv-iphone.telerama.fr/verytv/procedures/ListeChaines.php");  
		# ex�cute la requ�te et re�oit la r�ponse
	my $content = $ua->request($req);
	die $content->status_line if not $content->is_success;

	my $reponse= $content->content;
	my @content= split(/:\${3}:/,$reponse);                                                         # copier en lignes
#print @content;
	foreach my $ligne ( @content ){
		my ($num , $name)=($ligne =~ m{(.+)\${3}(.+)});
		
		if ( $name ){
			my$nameSD = CleanNom($name)  ;
			my $nameHD =  "$name HD" ;
			$nameHD =~ s/\s+//g;			# supprime les espaces			
			my ($xmltvid2, $xmltvid4)= ("","");

				$xmltvid2= "$num.telerama.fr" if  $num;
				$xmltvid4= "C$num.telerama.fr" if  $num;
#print "$nameSD \t $xmltvid2\t $xmltvid4 \n";
			
			foreach my $n  ( $nameSD, $nameHD ){
				$xmlid_map{$n}{xmltvid_iphone} = $xmltvid2 ;
				$xmlid_map{$n}{xmltvid_telerama} = $xmltvid4 ;
			}	
		}
	}
	return %xmlid_map;
}
#----------------------------------------------------------------------------------------------------------------------------
# recuperation des xmlid des fichiers du depot archlinux (pour Kazer, mc2xml, tv_grab_fr)
sub get_xmlid () {
	
	my@grabs = qw( kazer  mc2xml  tv_grab_fr );
	foreach my $grab ( @grabs)
		{
		my $content = get("http://download.tuxfamily.org/mythtvarch/icon_xmltvid/lookup_$grab.txt") or die("Unable to open file"); ;                # mettre le contenu dans une variable
		my @content= split('\n',$content);                                                         # copier en lignes
		foreach my $ligne ( @content )
		{
			my @cont = split ( '\|',$ligne);
			if (($cont[0])and ($cont[1])){
				my $name= $cont[0];
				my $nameSD = CleanNom($name) ;
				my $nameHD =  "$name HD" ;
				$nameHD =~ s/\s+//g;			# supprime les espaces	
				$cont[1]= "" if !($cont[1]);
#print "$cont[1]";				
				foreach my $n  ( $nameSD, $nameHD ){
					$xmlid_map{$n}{xmltvid_kazer} = $cont[1] if ($grab eq "kazer") ;				
					$xmlid_map{$n}{xmltvid_mc2xml}= $cont[1] if ($grab eq "mc2xml") ;
					$xmlid_map{$n}{xmltvid_telepoche}= $cont[1] if ($grab eq "tv_grab_fr") ;
#print "$xmlid_map{$n}{xmltvid_telepoche}\n";
				}
			}
		}
	}
	return %xmlid_map;
}

#----------------------------------------------------------------------------------------------------------------------------- 
# recuperation de la numerotation CanalSat
sub num_canalsat (){
#	($gridid , $option)= shift;
	my $url = ("http://www.lyngsat.com/packages/canalsatfr_chno.html");
	my $content1 = get("$url");                                                                                  # mettre le contenu dans une variable
	my @content1 = split(/<\/tr>/,$content1);                                                         # copier en lignes
	foreach my $ligne ( @content1 )
	{
		if ($ligne =~m{target=".*?">(.*)<\/a><\/b><\/td>})                                           # si le nom de chaine existe     
			{
			(my $name1) = ($ligne =~m{target=".*?">(.*?)<\/a><\/b><\/td>});            # extraire le nom
			$name1 = "$name1 HD" if ($ligne =~m{bgcolor=\"palegreen\"}) and !($name1 =~ m{HD});  # renommer les chaines HD
			my $name = CleanNom($name1)  ;
			(my $num) = ($ligne =~m{<b> *(\d{1,3})<\/b>});
			$num_map{$name}{canalsat} = $num ;
#			$configuration{Satellite}{$gridid}{base}{ $name}{numero_canalsat} =$num  if ($configuration{Satellite}{$gridid}{base}{ $name}and $num);  			# remplir la table de hash
			}

	}
	return %num_map;
}


#----------------------------------------------------------------------------------------------------------------------------
#recuperation de la liste des chaines d'un bouquet avec son nom, son icone et son sid sur le site de lyngsat
sub get_name_icone_sid () {
	my ($url, $gripid) =@_ ;
	my %icone_map;
	my $content = get("$url");                                                                                  # mettre le contenu dans une variable
	my @content = split(/<\/tr>/,$content);                                                         # copier en lignes
	foreach my $ligne ( @content )
	{
		if ($ligne =~m{target=".*?">(.*)<\/a><\/b>})                                           # si le nom de chaine existe     
			{
			(my $name1) = ($ligne =~m{target="homewin">(.*?)<\/a><\/b>});            # extraire le nom
			$name1 = "$name1 HD" if ($ligne =~m{bgcolor=\"palegreen\"}) and !($name1 =~ m{HD});  # renommer les chaines HD
			my $name = CleanNom($name1)  ;
			$name1 = CleanNom1($name1) ;
			my $sid = "";
			( $sid )= ($ligne =~m{<b> *(\d{3,5})<\/b><\/td});			# extraire le sid

			my $icone = "";
			($icone)= ($ligne =~m{img src="(.*?)" title=});                                # extraire l'icone
			$icone =~ s/\.gif/\.jpg/ if $icone;
			$icone =~ s/icon/logo/ if $icone;
			$sid = "" if ($sid eq "none");
			$icone_map {$name}= {'name' => $name1, 'icone' => $icone  , 'source' => "Hertzien" ,
				'option' => "" , 'bouquet'=> $gridid } if $gridid eq "tnt_fr" or $gridid eq "tntetrangere";
			$icone_map {$name}= {'name' => $name1, 'icone' => $icone , 'sid' => $sid , 'source' => "Satellite" ,
				'option' => "" , 'bouquet'=> $gridid  } if (!($gridid eq 'tnt_fr') and !($gridid eq 'tntetrangere')); 
			}
	}
	return %icone_map ;
}


#----------------------------------------------------------------------------------------------------------------------------
# nettoyage des noms de chaines pour comparaison entre les diffrentes sources

sub CleanNom
{
	my ($E)=shift;
	$E = NFKD $E; 											#supprime les accents
	$E =~ s/\pM//g; 											#supprime les accents											
	$E =uc $E ; 												# met en majuscule
	$E =~ s/13EME/13E/;										# renommer certains termes
	$E =~ s/EXTREM /EXTREME/;
	$E =~ s/ENCYCLOPEDIA/ENCYCLO/;
	$E =~ s/NAT GEO WILD/NATIONALGEOGRAPHICWILD/;
	$E =~ s/DE LA LOIRE/DE LOIRE/;
	$E =~ s/LA CHAINE PARLEMENTAIRE/LCP/;
	$E =~ s/SCIFI/SYFY UNIVERSAL/;
	my @i =  ( ' FRANCE', ' EUROPE', ' SAT', 'FRANCAIS', 'UK', 'AMERICA', ' INTERNATIONAL', '\(' , '\)', '\.', '\>', '\-', 'ENTERTAINMENT',
		 'LA CHAINE INFO', 'CHANNEL', 'OLYMPIQUE', 'LYONNAIS', 'FBS', 'ROUSSILLON');
	for my $i (@i)
		{
			$E =~ s/$i//;                                                                            # supprimer les termes definis dans @i
		}
	$E =~ s/\s+//g;										# supprime les espaces
	$E =~ s/.*RADIO.*//;										# suppression des radios
	return ( $E)
}

#----------------------------------------------------------------------------------------------------------------------------
# nettoyage des noms de chaines pour comparaison entre les diffrentes sources
sub CleanNom1
{
	my $E=shift;
	my @i =  ( ' France', " Fran.ais", ' Sat');
	for my $i (@i)
	{
		$E =~ s/$i//;                                                                            # supprimer les termes definis dans @i
	}
return ($E)	
}

#-----------------------------------------------------------------------------------------------------------------------------
# Give some information to the user
# Parameters:
#   current module
#   text to show to the user
sub say($) {
        my $question = shift;
	
        my$main_window1 = MainWindow->new;
$main_window1->focus;
        $main_window1->title("Information");
        $main_window1->minsize(qw(400 250));
        $main_window1->geometry('+250+150');

        my $top_frame    = $main_window1->Frame()->pack;
        my $middle_frame = $main_window1->Frame()->pack;
        my $bottom_frame = $main_window1->Frame()->pack(-side => 'bottom');
	
        $top_frame->Label(-height => 2)->pack;
        $top_frame->Label(-text => $question)->pack;
	
        $bottom_frame->Button(-text    => "OK",
			  -command => sub { $main_window1->destroy; },
			  -width    => 10,
			 )->pack(-padx => 2, -pady => 4);
	
        MainLoop();
}

#

#-------------------------------------------------------------------------------------------------------------------------------------
# Ask a yes/no question.
#
# Parameters:
#   current module
#   question text
#   default (true or false)
#
# Returns true or false, or undef if input could not be read.
#
sub ask_boolean($ ) {
        my $text =shift;
	
        my$main_window1 = MainWindow->new;

        $main_window1->title('Question');
        $main_window1->minsize(qw(400 250));
        $main_window1->geometry('+250+150');

        my$top_frame    = $main_window1->Frame()->pack;
        my $middle_frame = $main_window1->Frame()->pack;
        my $bottom_frame = $main_window1->Frame()->pack(-side => 'bottom');
	
        $top_frame->Label(-height => 2)->pack;
        $top_frame->Label(-text => $text)->pack;
	
        my $ans = 0;

        $bottom_frame->Button(-text    => "Yes",
			  -command => sub { $ans = 1; $main_window1->destroy; },
			  -width => 10,
			 )->pack(-side => 'left', -padx => 2, -pady => 4);
	
        $bottom_frame->Button(-text    => "No",
			  -command => sub { $ans = 0; $main_window1->destroy;},
			  -width => 10
			 )->pack(-side => 'left', -padx => 2, -pady => 4);
	
        MainLoop();
	
        return $ans;
}

1;
