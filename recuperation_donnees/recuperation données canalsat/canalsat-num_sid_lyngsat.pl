#!/usr/bin/env perl
use strict; 
use warnings;
use LWP;
use LWP::Simple;
use HTML::TreeBuilder::XPath;
use LWP::UserAgent;
#use WWW::Mechanize;
use Data::Dumper;

my $country= "France";
my $supplier = "CanalSat";
my $source = "DVB-S";
my %data;
# configurer le fichier de sauvegarde
	my $home = $ENV{HOME};
	$home = '.' if not defined $home;
	my $conf_dir = "$home";
	(-d $conf_dir) or mkdir($conf_dir, 0777)
	or die "cannot mkdir $conf_dir: $!";
	my $config_file ="canalsat_num_sid_lyngsat.txt";
	
# ouvrir le fichier de sauvegarde
	open(CONF, "> $config_file") or die "Cannot write to $config_file: $!";
	

#calcul de date
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $date =  $mday."/".($mon+1)."/".(1900+$year);


# recuperation des données

	my $url = 'http://www.lyngsat.com/packages/CanalSat-France_chno.html';
	my $url1 = 'http://www.lyngsat.com/packages/CanalSat-France_sid.html';


		print CONF "
# liste des chaines du bouquets CanalSat avec numero

# structure du fichier
#  pays,fournisseur,mode de diffusion,nom de chaine, numéro canalsat,sid,tp,adresse logo,clair ou crypté, sd ou hd
#
# crée le $date par Gilles Choteau à partir de la page 
#  $url
#  $url1

";

	my $browser = LWP::UserAgent->new;
	my $response = $browser->get($url);
	die "Can't get $url -- ", $response->status_line
		unless $response->is_success;
   
	my $tree = HTML::TreeBuilder->new_from_url($url);
	$tree->parse($response->content);

	my @elements = map { $_->parent } 
	  @{ $tree->findnodes('//td[@bgcolor]') };
	  print @elements;
	my @results;
	for my $element (@elements) {
	  push @results, [ map { $_->as_XML} $element->find('td') ]
	}

	my $x1 =0;
	for my $element (@elements) {
#		print Dumper $element;
		my $num= $element->findvalue( 'td[1]');
		   $num =~ s/\s+//g;										# supprime les espaces
		my $tp= $element->findvalue( 'td[2]');
		my $name= $element->findvalue( 'td[4]');
		my $icone= $element->findnodes_as_string( 'td[3]');
		  ($icone)= ($icone =~m{src="(.*?)"}); 
		my $enc= $element->findvalue( 'td[6]');
		my $format = 'sd';
		$format = 'hd' if ($enc=~m/HD/);
		$name = "$name hd" if ($enc=~m/HD/);
		if (!$enc) {
			$enc = 'clair'; 
		}else{
			
			$enc = 'crypté';
		}

                               # extraire l'icone
		if ($icone and $num and $num ne $x1 and $tp and $tp ne '24'and $tp =~ m/\d+/){
			$data{$name}{tp}=$tp;
			$data{$name}{num}=$num;
			$data{$name}{icone}=$icone;
			$data{$name}{enc}=$enc;
			$data{$name}{format}=$format;
			$data{$name}{sid}='1';
	}
#			$icone =~ s/\.gif/\.jpg/ if $icone;
#			$icone =~ s/icon/logo/ if $icone;
print "==========================================\n";
print "$name\t$num\t$tp\t$icone\n-----------------------------\n";
print "==========================================\n";
		$x1=$num;
	}
	$tree->delete; # to avoid memory leaks, if you parse many HTML documents 


#######################################################################################
#  sid
#######################################################################################

# recuperation des données

	


	$browser = LWP::UserAgent->new;
	$response = $browser->get($url1);
	die "Can't get $url1 -- ", $response->status_line
		unless $response->is_success;
   
	$tree = HTML::TreeBuilder->new_from_url($url1);
	$tree->parse($response->content);

	@elements = map { $_->parent } 
	  @{ $tree->findnodes('//td[@bgcolor]') };
	@results;
	for my $element (@elements) {
	  push @results, [ map { $_->as_XML} $element->find('td') ]
	}

	$x1 =0;
	for my $element (@elements) {
#		print Dumper $element;
		my $sid= $element->findvalue( 'td[1]');
		   $sid =~ s/\s+//g;										# supprime les espaces
		my $tp= $element->findvalue( 'td[2]');
		my $icone= $element->findnodes_as_string( 'td[3]');
		  ($icone)= ($icone =~m{src="(.*?)"}); 
		my $name= $element->findvalue( 'td[4]');
		my $enc= $element->findvalue( 'td[6]');
		$name = "$name hd" if ($enc=~m/HD/);
		my $format = 'sd';
		$format = 'hd' if ($enc=~m/HD/);
		if (!$enc) {
			$enc = 'clair'; 
		}else{
			$enc = 'crypté';
		}
		
                              
		if ($sid and $icone and $sid ne $x1 and $tp and $tp ne '24'and $tp =~ m/\d+/){
			$data{$name}{tp}=$tp;
			$data{$name}{sid}=$sid;
			$data{$name}{icone}=$icone;
			$data{$name}{enc}=$enc;
	}

		$x1=$sid;
	}
	$tree->delete; # to avoid memory leaks, if you parse many HTML documents 
	
####################################################################################












	
	for my $chanel (keys %data){
		print CONF "$country,$supplier,$source,$chanel,$data{$chanel}{num},$data{$chanel}{sid},$data{$chanel}{tp},$data{$chanel}{icone},$data{$chanel}{enc},$data{$chanel}{format}\n" ;	
	}

	print "Fichier sauvegardé sous $config_file\n";
	 
0;



__DATA__




<html>
<head>
<title>CanalSat France on Astra 1L/1M/1N at 19.2°E - LyngSat</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="keywords" content="CanalSat France on Astra 1L/1M/1N lyngsat satellite frequency chart transponder mux TV radio Lyngemark analog digital channel MPEG MPEG-2 MPEG-4 DVB DVB-S DVB-S2 DSS ISDB Digicipher ABS CBS2 FTA SR FEC SID PID PAL Viaccess Conax Irdeto Mediaguard Cryptoworks Videoguard RAS BISS PowerVu free">
<meta name="revisit-after" content="1 days">
<base target="_top">
<link href="../misc/style.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor='moccasin' text='#000000' link='blue' vlink='blue'>

<div align="center">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-17822891-1', 'lyngsat.com');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');

</script><!-- big table start -->
<table width=1048 border=0 cellspacing=0 cellpadding=0>
<tr>
<td width="160" align="center" valign="top">
<table border=0 cellspacing=1 cellpadding=1>
<tr><td align='center'><font face='Arial' size=2><a href='http://www.lyngsat.com/advert.html'><font color='blue'><b>Advertising</b></font></a></font></td></tr><tr align='center'>
<td width=160 height=160><a href='http://www.lyngsat.info/adverts/redirect.php?url=http://www.intelsat.com/about-us/50th-anniversary/&pg=www.lyngsat.com/packages/CanalSat-France_chno.html&im=intelsat_anniversary.jpg&pl=L0' target='advertwin'><img src='../adverts/images/intelsat_anniversary.jpg' width=160 height=160 border=0></a></td>
</tr>
<tr align='center'>
<td width=160 height=160><a href='http://www.lyngsat.info/adverts/redirect.php?url=http://www.viewsat.eu/&pg=www.lyngsat.com/packages/CanalSat-France_chno.html&im=viewsat_1405v2.gif&pl=L160' target='advertwin'><img src='../adverts/images/viewsat_1405v2.gif' width=160 height=160 border=0></a></td>
</tr>
<tr align='center'>
<td width=160 height=600><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- LyngSat 160x600 #1 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:160px;height:600px"
     data-ad-client="ca-pub-2600709618101011"
     data-ad-slot="3945607514"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></td>
</tr>
</table>
</td>
<td width=728 align="center" valign="top">
<p>

<table width=720 border=0 cellspacing=1 cellpadding=1>
<tr valign="top">
<td align="center" bordercolor=moccasin bgcolor=moccasin><font face="Arial" size=2>
<a href="http://www.lyngsat.com/"><img src="../images/lyngsat_210x48.gif" width=210 height=48 border=0></a>
<br><br>
<b><font face="Arial" size=5>CanalSat France on Astra 1L/1M/1N at 19.<font face="Arial" size=3>2</font><font face="Arial" size=5>°E</font></font></b>
</td>
</tr>
<tr valign="baseline">
<td align="center" width=720>
<table width=728 border=0 cellspacing=1 cellpadding=1><tr><td align='center' width=728 height=90><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- LyngSat 728x90 #1 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9851824455959986"
     data-ad-slot="0188404002"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></td>
</tr></table></td>
</tr>
</table>
<table width=720 border=0 cellspacing=0 cellpadding=0>
<tr>
<td><font size=3 color="#ff9C00"><b><img src="http://www.lyngsat.com/images/pixel_brun.gif" width=720 height=1 vspace=4></b></td>
</tr>
</table>
<table width=720 border=0 cellspacing=0 cellpadding=0 bgcolor="khaki">
<tr align="center">
<td align="left"><font face="Arial" size=2><b><a href="http://www.lyngsat.com/packages/asia.html">Asia <img src="http://www.lyngsat.com/p.gif" height=15 width=15 align="absmiddle"></a></b></td>
<td align="center"><font face="Arial" size=2 color="#000000">
<b><a href="http://www.lyngsat.com/index.html">Main</a>
 | <a href="http://www.lyngsat.com/europe.html">Europe</a>
 | <a href="http://www.lyngsat.com/packages/europe.html">Europe <img src="http://www.lyngsat.com/p.gif" height=15 width=15 align="absmiddle"></a>
 | <a href="http://www.lyngsat.com/hd/europe.html">Europe  <img src="http://www.lyngsat.com/hd.gif" height=15 width=25 align="absmiddle"></a>
 | <a href="http://www.lyngsat.com/tracker/europe.html">Europe <img src="http://www.lyngsat.com/map-mini.gif" height=15 width=30 align="absmiddle"></a>
 | <a href="http://www.lyngsat.com/3d/index.html">3D</a>
 | <a href="http://www.lyngsat.com/headlines.html">Headlines</a>
</td>
<td align="right"><font face="Arial" size=2><b><a href="http://www.lyngsat.com/packages/atlantic.html">Atlantic <img src="http://www.lyngsat.com/p.gif" height=15 width=15 align="absmiddle"></a></b></td>
</tr>
</table>
<table width=720 border=0 cellspacing=0 cellpadding=0>
<tr>
<td><font size=3 color="#ff9C00"><b><img src="http://www.lyngsat.com/images/pixel_brun.gif" width=720 height=1 vspace=4></b></td>
</tr>
</table>
<table width=720 border=0 cellspacing=0 cellpadding=0 bgcolor="khaki">
<tr align="center">
<td align="center"><font face="Arial" size=2 color="#000000">
<b><a href="http://www.lyngsat.com/packages/CanalSat-France.html">sorted by frequency</a>
 | <a href="http://www.lyngsat.com/packages/CanalSat-France_sid.html">sorted by SID</a>
 | <a href="http://www.lyngsat.com/packages/CanalSat-France_chno.html">sorted by channel number</a>
</b>
</td>
</tr>
</table>
<table width=720 border=0 cellspacing=0 cellpadding=0>
<tr>
<td><font size=3 color="#ff9C00"><b><img src="http://www.lyngsat.com/images/pixel_brun.gif" width=720 height=1 vspace=4></b></td>
</tr>
</table>
<table border=0 cellspacing=0 cellpadding=0><tr><td><font face="Verdana" size=2 color="black"><i>The EIRP values are for Greece</i></td></tr></table>

<table width=720 border cellspacing=0 cellpadding=0>
<tr>
<td colspan=8 align="center"><font face="Arial" size=2>CanalSat France &#169; LyngSat, last updated 2014-07-20 - http://www.lyngsat.com/packages/CanalSat-France_chno.html</td>
</tr>
<tr>
<td width=70 align="center"><font face="Verdana" size=2>Ch</td>
<td width=50 align="center"><font face="Verdana" size=2>Tp</td>
<td width=39 align="center"><br></td>
<td width=220 align="left"><font face="Verdana" size=2>Channel Name</td>
<td width=15><br></td>
<td width=90 align="left"><font face="Verdana" size=1><font size=1>Enc. system</td>
<td width=50 align="left"><font face="Verdana" size=1>Beam<br>EIRP&nbsp;(dBW)<br><font color="green">C/N lock</font></td>
<td width=80 align="left"><font face="Verdana" size=1>Source<br>Updated</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    0</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_mosaique_1.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Mosaique-1.html">Mosaique 1</a></b></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140219</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    0</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_mosaique_2.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Mosaique-2.html">Mosaique 2</a></b></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140219</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    0</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_mosaique_3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Mosaique-3.html">Mosaique 3</a></b></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140219</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    0</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_mosaique_5.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Mosaique-5.html">Mosaique 5</a></b></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140219</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    0</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_mosaique_4.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Mosaique-4.html">Mosaique 4</a></b></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140219</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    1</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/TF1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tf1.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/TF-1.html">TF 1</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/TF1.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>A Aquina<br>130818</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    2</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-2.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france2.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-2.html">France 2</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/France2.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    3</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Sat.html">France 3 Sat</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Sat.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    4</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_clair.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---France.html">Canal + France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Canal+.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    5</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-5.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france5.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-5.html">France 5</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-5.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    6</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/M6.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/m6_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/M6.html">M6</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/ln/M6.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    7</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/ARTE-Francais.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/aa/arte_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/ARTE-Francais.html">ARTE Français</a></b></td>
<td bgcolor="#ffffbb"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/ab/ARTE-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>A Aquina<br>130818</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>    8</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_mosaique.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Mosaique-Canal--.html">Mosaique Canal +</a></b></td>
<td bgcolor="#ffcc99"><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td bgcolor="#d0ffff"><font face="Verdana" size=1>N Schlammer<br>140710</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   10</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---Cinema-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cinema.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---Cinema-France.html">Canal + Cinéma France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Canal+Cinema.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   11</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---Sport-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_sport.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---Sport-France.html">Canal + Sport France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Canal+Sport.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   12</b></td>
<td align="center"><font face="Verdana" size=1>80</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---Series.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_series.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---Series.html">Canal + Séries</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Canal+Series-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>Pr2<br>130923</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   13</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---Family-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_family.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---Family-France.html">Canal + Family France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Canal+Family.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   14</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---Decale.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_decale.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---Decale.html">Canal + Décalé</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Canal+Decale.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   14</b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---a-la-demande.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_demande.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b>Canal + à la demande promo</b></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   17</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_fr_a_la_une.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/A-la-une.html">A la une</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/A-la-Une-Canal+.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130928</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   18</b></td>
<td align="center"><font face="Verdana" size=1>18</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/13eme-Rue.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/num/13eme_rue_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/13eme-Rue.html">13ème Rue</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/uz/13emeRue-CSN.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>Karl<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   19</b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Comedie--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_comedie_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Comedie--.html">Comédie +</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Comedie.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   20</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine---Premier.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cine_plus_premier.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cine---Premier.html">Cine + Premier</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Cine+Premier.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   21</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine---Frisson.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cine_plus_frisson.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cine---Frisson.html">Cine + Frisson</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Cine+Frisson.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   22</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine---Emotion.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cine_plus_emotion.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cine---Emotion.html">Cine + Émotion</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Cine+Emotion.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   23</b></td>
<td align="center"><font face="Verdana" size=1>106</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine---Famiz.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cine_plus_famiz.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cine---Famiz.html">Cine + Famiz</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Cine+Famiz.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121114</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   24</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine---Club.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cine_plus_club.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cine---Club.html">Cine + Club</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Cine+Club.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   25</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine---Classic.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cine_plus_classic.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cine---Classic.html">Cine + Classic</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Cine+Classic.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   26</b></td>
<td align="center"><font face="Verdana" size=1>106</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/TCM-Cinema.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tcm_cinema_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/TCM-Cinema.html">TCM Cinéma</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/TCM-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140513</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   27</b></td>
<td align="center"><font face="Verdana" size=1>92</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Paramount-Channel-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/paramount_channel_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Paramount-Channel-France.html">Paramount Channel France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/or/Paramount-Channel-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>140504</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   33</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine-Polar.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/cine_polar.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Polar.html">Polar</a></b> </font><font size=1>(France)</td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Cine-Polar.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   34</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine-FX.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/cine_fx.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cine-FX.html">Ciné FX</a></b></td>
<td bgcolor="#99ff99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/cc/Cine-FX.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   35</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Action.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/aa/action_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Action.html">Action</a></b> </font><font size=1>(France)</td>
<td bgcolor="#99ff99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/ab/Action.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   36</b></td>
<td align="center"><font face="Verdana" size=1>114</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Disney-Cinemagic-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/disney_cinemagic_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Disney-Cinemagic-France.html">Disney Cinemagic France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Disney-Cinemagic-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>090526</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   37</b></td>
<td align="center"><font face="Verdana" size=1>114</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Disney-Cinemagic-France--1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/disney_cinemagic_fr_plus1.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Disney-Cinemagic-France--1.html">Disney Cinemagic France +1</a></b></td>
<td bgcolor="#ffcc99"><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>090526</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   40</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Jimmy.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/jj/jimmy_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Jimmy.html">Jimmy</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/gk/Jimmy-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   42</b></td>
<td align="center"><font face="Verdana" size=1>18</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Syfy-Universal-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ss/syfy_universal_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Syfy-France.html">Syfy France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ss/Syfy-Uni-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>Karl<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   43</b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/TF6.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tf6.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/TF-6.html">TF 6</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/TF6.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   44</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Serie-Club.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ss/serie_club.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Serie-Club.html">Série Club</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ss/Serie-Club-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   45</b></td>
<td align="center"><font face="Verdana" size=1>66</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/au/MTV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mtv_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/MTV-France.html">MTV France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MTV-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Cryptoworks<br>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100204</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   46</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/June.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/jj/june.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/June.html">June</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/gk/June-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   47</b></td>
<td align="center"><font face="Verdana" size=1>84</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Non-Stop-People.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/non_stop_people_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Non-Stop-People.html">Non Stop People</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/Non-Stop-People.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe.html">Europe</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130106</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   48</b></td>
<td align="center"><font face="Verdana" size=1>114</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/E-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/e_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/E-France.html">E! France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/E-Ent-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>A Cartledge<br>071111</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   49</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MCM-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mcm_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/MCM-France.html">MCM France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MCM.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>131118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   50</b></td>
<td align="center"><font face="Verdana" size=1>86</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/J-One-TV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/jj/j_one_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/J-One.html">J-One</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/gk/J-One-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>Pr2<br>131008</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   51</b></td>
<td align="center"><font face="Verdana" size=1>66</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Game-One.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/gg/game_one.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Game-One.html">Game One</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/gk/Game-One.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Cryptoworks<br>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100209</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   52</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Mangas.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mangas.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Mangas.html">Mangas</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/ln/Mangas-ABSat.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   53</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/be/AB1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/aa/ab1.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/AB-1.html">AB 1</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/ab/AB-1.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   54</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/TV-Breizh.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tv_breizh_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/TV-Breizh.html">TV Breizh</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/TV-Breizh.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   55</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Teva.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/teva_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Teva.html">Téva</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/Teva.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>140504</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   56</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Paris-Premiere.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/paris_premiere.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Paris-Premiere.html">Paris Première</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/Paris-Premiere.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   57</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/RTL-9.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/rr/rtl9.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/lu/RTL-9.html">RTL 9</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/or/RTL9.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   58</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/D-8.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_d8.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/D8.html">D8</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Canal+D8.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   59</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/W9.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ww/w9_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/W9.html">W9</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/uz/W9.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>A Aquina<br>130818</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   60</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/mc/TMC--Tele-Monte-Carlo.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tmc.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/mc/TMC.html">TMC</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/TMC-Monte-Carlo.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   61</b></td>
<td align="center"><font face="Verdana" size=1>108</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/NT1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/nt1.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/NT1.html">NT1</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/ln/NT1-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>Martin cz<br>R László 2<br>140506</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   62</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/NRJ-12.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/nrj12.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/NRJ-12.html">NRJ 12</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/NRJ-Hits.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   63</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-4.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france4.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-4.html">France 4</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/France-4.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>140504</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   65</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-O.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france_o.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-O.html">France Ô</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/France-O.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   68</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Numero-23.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/numero_23_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Numero-23.html">Numero 23</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/TF1-Numero-23.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121213</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   71</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/t_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tv5_monde_fr_be_ch.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/TV-5-Monde-France-Belgique-Suisse.html">TV 5 Monde France Belgique Suisse</a></b></td>
<td bgcolor="#ffffbb"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/tt/TV5-Monde-FBS.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   75</b></td>
<td align="center"><font face="Verdana" size=1>112</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cuisine--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cuisine_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cuisine--.html">Cuisine +</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Cuisine-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   76</b></td>
<td align="center"><font face="Verdana" size=1>106</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Maison--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_maison_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Maison--.html">Maison +</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/Maison-Plus.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121114</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   78</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MCS-Bien-etre.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/ma_chaine_sport_bien_etre.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/MCS-Bien-Etre.html">MCS Bien-Etre</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MCS-Bien-Etre.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>120405</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   80</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Planete--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/planete_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Planete--.html">Planète +</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/Planete-CSN.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   81</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Planete---Thalassa.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/planete_plus_thalassa.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Planete---Thalassa.html">Planète + Thalassa</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/Planete-Thalassa.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>F Rotsikas<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   82</b></td>
<td align="center"><font face="Verdana" size=1>112</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Planete---CI.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/planete_plus_crime_investigation.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Planete---CI.html">Planète + CI</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/Planete-CI.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>081104</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   83</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Planete---AandE.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/planete_plus_action_experience.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Planete---AandE.html">Planète + A&E</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/Planete-A-and-E.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   84</b></td>
<td align="center"><font face="Verdana" size=1>112</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Discovery-Channel.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/discovery_international.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Discovery-Channel-France.html">Discovery Channel France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Discovery-Channel-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   86</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/National-Geographic-Channel.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/nat_geo_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/National-Geographic-Channel-France.html">National Geographic Channel France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/NGC-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   87</b></td>
<td align="center"><font face="Verdana" size=1>102</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ar/Nat-Geo-Wild-US.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/nat_geo_wild_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Nat-Geo-Wild-France.html">Nat Geo Wild France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/NG-Wild-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>T Viererbe<br>120626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   88</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Voyage.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/vv/voyage.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Voyage.html">Voyage</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/uz/Voyage.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   89</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Ushuaia-TV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/uu/ushuaia_tv.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Ushuaia-TV.html">Ushuaïa TV</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/uz/Ushuaia-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   90</b></td>
<td align="center"><font face="Verdana" size=1>112</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Histoire.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/hh/histoire_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Histoire.html">Histoire</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/gk/Histoire.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>F Rotsikas<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   91</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Toute-lHistoire.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/toute_histoire.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Toute-lHistoire.html">Toute l'Histoire</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/tt/Toute-Histoire.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   92</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Animaux.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/aa/animaux.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Animaux.html">Animaux</a></b></td>
<td bgcolor="#99ff99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/ab/Animaux.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   93</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Escales.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/escales.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Escales.html">Escales</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/Escales.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   94</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Encyclo.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/encyclo_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Encyclo.html">Encyclo</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/Encyclopedia.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   95</b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/s_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ss/seasons.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Seasons.html">Seasons</a></b> </font><font size=1>(11-08)</td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ss/Seasons-CSN.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   97</b></td>
<td align="center"><font face="Verdana" size=1>108</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/8-Mont-Blanc.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/num/8_mont_blanc_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/8-Mont-Blanc.html">8 Mont Blanc</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/tt/TV-8-Mont-Blanc.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140214</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>   98</b></td>
<td align="center"><font face="Verdana" size=1>26</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/RMC-Decouverte-HD-24.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/rr/rmc_decouverte.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/RMC-Decouverte-HD-24.html">RMC Découverte HD 24</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/RMC-Decouverte.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121213</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  100</b></td>
<td align="center"><font face="Verdana" size=1>114</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/l_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ll/lachainemeteo.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/La-Chaine-Meteo.html">La Chaîne Météo</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/La-Chaine-Meteo.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  101</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/LCI.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ll/lci.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/LCI.html">LCI</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/LCI-La-Chaine-Info.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  102</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/I-Tele.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ii/i_tele.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/I-Tele.html">I>Télé</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/fr/I-Tele.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/gk/I-Tele.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  103</b></td>
<td align="center"><font face="Verdana" size=1>108</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/BFM-TV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/bfm_tv.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/BFM-TV.html">BFM TV</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/fr/BFM-TV.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ab/BFM-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>100111</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  104</b></td>
<td align="center"><font face="Verdana" size=1>91</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/EuroNews.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/euronews_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/EuroNews.html">EuroNews</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/fr/EuroNews.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/EuroNews.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>101221</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  105</b></td>
<td align="center"><font face="Verdana" size=1>22</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/France-24-Francais.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france24_francais.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-24-Francais.html">France 24 Français</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/fr/France-24-Francais.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/France-24.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1KR-Europe.html">Europe</a><br>0-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>F Dibba<br>061207</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  106</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/LCP.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ll/lcp_an_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/LCP.html">LCP</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/fr/LCP.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ln/LCP-AN.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>A Aquina<br>130818</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  106</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Public-Senat.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/public_senat.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Public-Senat.html">Public Sénat</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/fr/Public-Senat.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Public-Senat.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>A Aquina<br>130818</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  107</b></td>
<td align="center"><font face="Verdana" size=1>108</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/BFM-Business.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/bfm_business.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/BFM-Business.html">BFM Business</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/fr/BFM-Business.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ab/BFM-Business.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>K Bamik<br>110919</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  108</b></td>
<td align="center"><font face="Verdana" size=1>26</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/us/Bloomberg-TV-US.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/bloomberg_tv_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Bloomberg-TV-Europe.html">Bloomberg TV Europe</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/us/Bloomberg-TV-Europe.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ab/BloombergTV-Europe.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/United-States.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091117</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  109</b></td>
<td align="center"><font face="Verdana" size=1>68</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/uk/CNN-International-Europe.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/cnn_international_europe.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/CNN-International-Europe.html">CNN International Europe</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/cc/CNN-International-Europe.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/United-States.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>W Zaremba<br>021130</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  110</b></td>
<td align="center"><font face="Verdana" size=1>26</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/uk/BBC-World-News.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/bbc_world_news.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/uk/BBC-World-News-Europe.html">BBC World News Europe</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/ab/BBC-World-News.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/United-Kingdom.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>J Puig<br>050201</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  111</b></td>
<td align="center"><font face="Verdana" size=1>26</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/de/CNBC-Europe.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/cnbc_europe.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/CNBC-Europe.html">CNBC Europe</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/cc/CNBC-Europe.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/United-States.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>DXHuzy<br>040713</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  112</b></td>
<td align="center"><font face="Verdana" size=1>40</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/il/I-24-News.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ii/i_24_news_il.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/il/I24-News-Francais.html">I24 News Français</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/il/I24-News-Francais.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/gk/I24-News-Francais.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/Israel.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130718</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  120</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Sport--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_sport_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Sport--.html">Sport +</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ss/Sport+.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  121</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/at/EuroSport.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/eurosport.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Eurosport-France.html">Eurosport France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Eurosport-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>A Aquina<br>130818</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  122</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/InfoSport--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_infosport_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/InfoSport--.html">InfoSport +</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/gk/Infosport.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  123</b></td>
<td align="center"><font face="Verdana" size=1>12</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/LEquipe-21.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ll/l_equipe_21_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/LEquipe-21.html">L'Équipe 21</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/LEquipe-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130328</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  124</b></td>
<td align="center"><font face="Verdana" size=1>106</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Sport-365.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ss/sport_365_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Sport-365.html">Sport 365</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ss/Sport-365-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>T Viererbe<br>130910</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  125</b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/be/EuroSport-2.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/eurosport2.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Eurosport-2.html">Eurosport 2</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Eurosport-2.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  126</b></td>
<td align="center"><font face="Verdana" size=1>112</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Ma-Chaine-Sport.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/ma_chaine_sport.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Ma-Chaine-Sport.html">Ma Chaîne Sport</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/Ma-Chaine-Sport.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>081104</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  127</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MCS-Extreme.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/ma_chaine_sport_extreme.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/MCS-Extreme.html">MCS Extrême</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MCS-Extreme.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>120124</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  129</b></td>
<td align="center"><font face="Verdana" size=1>114</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Equidia-Live.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/equidia_live.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Equidia-Live.html">Equidia Live</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Equidia-Live.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  130</b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Equidia-Life.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/equidia_life.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Equidia-Life.html">Equidia Life</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Equidia-Life.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  131</b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/be/Motors-TV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/motors_tv_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Motors-TV.html">Motors TV</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/Motors-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  132</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/OMTV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/oo/om_tv.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/OMTV.html">OMTV</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/OMTV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>F Rotsikas<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  133</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/OLTV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/oo/ol_tv.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/OLTV.html">OLTV</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/OL-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>F Rotsikas<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  134</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Onzeo.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/oo/onzeo_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Onzeo.html">Onzéo</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/Onzeo-Asse-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  135</b></td>
<td align="center"><font face="Verdana" size=1>18</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Girondins-TV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/gg/girondins_tv.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Girondins-TV.html">Girondins TV</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/gk/Girondins-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>101219</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  138</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/it/Nautical-Channel.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/nautical_channel.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/it/Nautical-Channel.html">Nautical Channel</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/Nautical-Channel.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>Denis 2<br>140410</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  139</b></td>
<td align="center"><font face="Verdana" size=1>22</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/qa/BeIn-Sports-1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_1.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-1.html">BeIn Sports 1</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-1.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1KR-Europe.html">Europe</a><br>0-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>120815</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  140</b></td>
<td align="center"><font face="Verdana" size=1>22</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/qa/BeIn-Sports-2.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_2.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-2.html">BeIn Sports 2</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-2.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1KR-Europe.html">Europe</a><br>0-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>120815</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  141</b></td>
<td align="center"><font face="Verdana" size=1>106</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Golf--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_golf_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Golf--.html">Golf +</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/CanalSat-Golf+.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>Alex Sosa<br>120902</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  142</b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Extreme-Sports-Channel-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/extreme_sports_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/nl/Extreme-Sports-Channel-France.html">Extreme Sports Channel France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Extreme-Sports-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  144</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/AB-Moteurs.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/aa/ab_moteurs.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/AB-Moteurs.html">AB Moteurs</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/ab/AB-Moteurs.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  150</b></td>
<td align="center"><font face="Verdana" size=1>114</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Disney-Junior.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/disney_junior_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Disney-Junior-France.html">Disney Junior France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Disney-Junior-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>090526</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  151</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/TiJi.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tiji.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/TiJi.html">TiJi</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/TiJi.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  152</b></td>
<td align="center"><font face="Verdana" size=1>78</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Nickelodeon-Junior-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/nickelodeon_junior_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Nickelodeon-Junior-France.html">Nickelodeon Junior France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/Nick-Jr-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100209</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  154</b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Piwi--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_piwi_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Piwi--.html">Piwi +</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/Piwi.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  155</b></td>
<td align="center"><font face="Verdana" size=1>114</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Boomerang.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/boomerang_global.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Boomerang-France.html">Boomerang France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/Boomerang-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140513</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  156</b></td>
<td align="center"><font face="Verdana" size=1>66</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/at/Nickelodeon.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/nickelodeon_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Nickelodeon-France.html">Nickelodeon France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/Nickelodeon-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Cryptoworks<br>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100209</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  157</b></td>
<td align="center"><font face="Verdana" size=1>112</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal-J.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canal_j.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal-J.html">Canal J</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Canal-J.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  158</b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Disney-Channel.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/disney_channel_global.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Disney-Channel-France.html">Disney Channel France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Disney-Channel-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  159</b></td>
<td align="center"><font face="Verdana" size=1>114</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Disney-Channel.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/disney_channel_global.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Disney-Channel-France--1.html">Disney Channel France +1</a></b></td>
<td bgcolor="#ffcc99"><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>W Zaremba<br>040914</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  160</b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Cartoon-Network.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/cartoon_network_global.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Cartoon-Network-France.html">Cartoon Network France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Cartoon-Network-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140513</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  160</b></td>
<td align="center"><font face="Verdana" size=1>86</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Cartoon-Network.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/cartoon_network_global.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Cartoon-Network-France.html">Cartoon Network France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/cc/Cartoon-Network-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  161</b></td>
<td align="center"><font face="Verdana" size=1>114</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Disney-XD.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/disney_xd_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Disney-XD-France.html">Disney XD France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Disney-XD-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  162</b></td>
<td align="center"><font face="Verdana" size=1>114</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/TeleToon--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_teletoon_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/TeleToon--.html">TéléToon +</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/TeleToon.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130524</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  163</b></td>
<td align="center"><font face="Verdana" size=1>18</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/TeleToon--1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_teletoon_plus1.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/TeleToon---1.html">TéléToon + 1</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/TeleToon-1.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  164</b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Boing-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/boing_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/it/Boing-France.html">Boing France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/Boing-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140513</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  165</b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Gulli.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/gg/gulli.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Gulli.html">Gulli</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/gk/Gulli-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>A Aquina<br>130818</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  170</b></td>
<td align="center"><font face="Verdana" size=1>66</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MTV-Base.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mtv_base_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/MTV-Base-France.html">MTV Base France</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MTV-Base-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Cryptoworks<br>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100209</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  171</b></td>
<td align="center"><font face="Verdana" size=1>78</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MTV-Pulse.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mtv_pulse.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/MTV-Pulse.html">MTV Pulse</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MTV-Pulse-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>100811</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  172</b></td>
<td align="center"><font face="Verdana" size=1>78</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MTV-Idol.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mtv_idol.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/MTV-Idol.html">MTV Idol</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MTV-Idol.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>100811</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  174</b></td>
<td align="center"><font face="Verdana" size=1>106</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/be/Trace-Urban-Francais.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/trace_urban_french.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Trace-Urban-Francais.html">Trace Urban Français</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/Trace-Urban-Francais.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121114</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  175</b></td>
<td align="center"><font face="Verdana" size=1>18</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/NRJ-Hits.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/nrj_hits.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/NRJ-Hits.html">NRJ Hits</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-stream.com/radiochannels/fr/NRJ-Hits.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ln/NRJ-Hits.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>101219</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  176</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MCM-Top.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mcm_top.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/MCM-Top.html">MCM Top</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MCM-Top.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  177</b></td>
<td align="center"><font face="Verdana" size=1>74</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MCM-Pop.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mcm_pop.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/MCM-Pop.html">MCM Pop</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MCM-Pop.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  178</b></td>
<td align="center"><font face="Verdana" size=1>86</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/nl/Brava-HD.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/brava_hdtv.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/nl/Brava-HD.html">Brava HD</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ab/Brava-HDTV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140417</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  180</b></td>
<td align="center"><font face="Verdana" size=1>66</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/us/MTV-Hits-US.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mtv_hits_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/MTV-Hits.html">MTV Hits</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MTV-Hits.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Conax<br>Cryptoworks<br>Irdeto 2<br>Mediaguard 2<br>Nagravision 3<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>130701</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  181</b></td>
<td align="center"><font face="Verdana" size=1>66</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/at/MTV-Rocks.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mtv_rocks_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/MTV-Rocks.html">MTV Rocks</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/MTV-Rocks-UK.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Conax<br>Cryptoworks<br>Mediaguard 2<br>Nagravision 3<br>Viaccess 4.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>120202</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  182</b></td>
<td align="center"><font face="Verdana" size=1>18</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/M6-Music.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/m6_music_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/M6-Music.html">M6 Music</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/M6-Music-Hits.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  184</b></td>
<td align="center"><font face="Verdana" size=1>112</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/M6-Music-Black.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/m6_music_black.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/M6-Music-Black.html">M6 Music Black</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/M6-Music-Black.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>081104</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  185</b></td>
<td align="center"><font face="Verdana" size=1>112</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/M6-Music-Club.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/m6_music_club.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/M6-Music-Club.html">M6 Music Club</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/M6-Music-Club.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>090120</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  188</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Melody.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/melody_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Melody.html">Mélody</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/tt/Tele-Melody.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>F Rotsikas<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  189</b></td>
<td align="center"><font face="Verdana" size=1>72</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Mezzo.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mezzo.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Mezzo.html">Mezzo</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/Mezzo.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 5.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130416</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  190</b></td>
<td align="center"><font face="Verdana" size=1>102</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/be/Mezzo-Live-HD.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mezzo_live_hd.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Mezzo-Live-HD.html">Mezzo Live HD</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/Mezzo-HD.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>120124</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  200</b></td>
<td align="center"><font face="Verdana" size=1>26</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/M6-Boutique-and-Co.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/m6_boutique_and_co.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/M6-Boutique-and-Co.html">M6 Boutique & Co</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/fr/M6-Boutique-and-Co.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ln/M6-Boutique.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Berecz<br>130524</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  201</b></td>
<td align="center"><font face="Verdana" size=1>26</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/b_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/best_of_shopping.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Best-of-Shopping.html">Best of Shopping</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/ab/Best-of-Shopping.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>110302</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  203</b></td>
<td align="center"><font face="Verdana" size=1>108</td>
<td align="center"><br><br>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2 color="gray"><b>[Cash TV info card]</b></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140325</td>
</tr>
tr>
<td align="center"><font face="Verdana" size=2><b>  211</b></td>
<td align="center"><font face="Verdana" size=1>18</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/French-Lover-TV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/french_lover_tv.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/at/French-Lover-TV.html">French Lover TV</a></b> </font><font size=1>(23-04)</td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/FrenchLover-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  212</b></td>
<td align="center"><font face="Verdana" size=1>18</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/l_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ll/libido_tv.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Libido-TV.html">Libido TV</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ln/Libido-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>110310</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  213</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/d_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/dorcel_tv.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/nl/Dorcel-TV.html">Dorcel TV</a></b> </font><font size=1>(22-05)</td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/Dorcel-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  214</b></td>
<td align="center"><font face="Verdana" size=1>94</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/x_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/xx/xxl_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/XXL.html">XXL</a></b> </font><font size=1>(22.30-03.00)</td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/uz/XXL.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140514</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  216</b></td>
<td align="center"><font face="Verdana" size=1>86</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/us/Penthouse-HD.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/penthouse_hd.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Penthouse-HD.html">Penthouse HD</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/or/Penthouse-HD.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130910</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  217</b></td>
<td align="center"><font face="Verdana" size=1>18</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Pink-TV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/pink_tv_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Pink-TV.html">Pink TV</a></b> </font><font size=1>(22-05)</td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/or/PinkTV-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  250</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_a_la_carte_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/A-la-carte-1.html">A la carte 1</a></b></td>
<td bgcolor="#ffcc99"><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  251</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Foot--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_fr_foot_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Foot--.html">Foot +</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/CanalSat-Foot+.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  252</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_a_la_carte_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/A-la-carte-2.html">A la carte 2</a></b></td>
<td bgcolor="#ffcc99"><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  254</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_a_la_carte_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/A-la-carte-3.html">A la carte 3</a></b></td>
<td bgcolor="#ffcc99"><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  255</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_a_la_carte_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/A-la-carte-4.html">A la carte 4</a></b></td>
<td bgcolor="#ffcc99"><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  256</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_a_la_carte_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/A-la-carte-5.html">A la carte 5</a></b></td>
<td bgcolor="#ffcc99"><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  257</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_a_la_carte_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/A-la-carte-6.html">A la carte 6</a></b></td>
<td bgcolor="#ffcc99"><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  258</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_a_la_carte_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/A-la-carte-7.html">A la carte 7</a></b></td>
<td bgcolor="#ffcc99"><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br> MPEG-4</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  280</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/BeIn-Sports-Max.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_max.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-Max-3.html">BeIn Sports Max 3</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-Max-3.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  281</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/BeIn-Sports-Max.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_max.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-Max-4.html">BeIn Sports Max 4</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-Max-4.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  282</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/BeIn-Sports-Max.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_max.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-Max-5.html">BeIn Sports Max 5</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-Max-5.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  283</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/BeIn-Sports-Max.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_max.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-Max-6.html">BeIn Sports Max 6</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-Max-6.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  284</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/BeIn-Sports-Max.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_max.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-Max-7.html">BeIn Sports Max 7</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-Max-7.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  285</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/BeIn-Sports-Max.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_max.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-Max-8.html">BeIn Sports Max 8</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-Max-8.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  286</b></td>
<td align="center"><font face="Verdana" size=1>116</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/BeIn-Sports-Max.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_max.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-Max-9.html">BeIn Sports Max 9</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-Max-9.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  350</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Alpes.html">France 3 Alpes</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  351</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Alsace.html">France 3 Alsace</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Alsace.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  352</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Aquitaine.html">France 3 Aquitaine</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  353</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Auvergne.html">France 3 Auvergne</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  354</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Basse-Normandie.html">France 3 Basse-Normandie</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  355</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Bourgogne.html">France 3 Bourgogne</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  356</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Bretagne.html">France 3 Bretagne</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Bretagne.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  357</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Centre.html">France 3 Centre</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Centre.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  358</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Champagne-Ardenne.html">France 3 Champagne-Ardenne</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Champagne-Ardenne.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  359</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Corse-Via-Stella.html">France 3 Corse Via Stella</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Corse-ViaStella.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  360</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Cote-dAzur.html">France 3 Côte d'Azur</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121116</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  361</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Franche-Comte.html">France 3 Franche-Comté</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  362</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Haute-Normandie.html">France 3 Haute-Normandie</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Haute-Normandie.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  363</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Languedoc.html">France 3 Languedoc</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  364</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Limousin.html">France 3 Limousin</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  365</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Lorraine.html">France 3 Lorraine</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Lorraine.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  366</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Midi-Pyrenees.html">France 3 Midi-Pyrénées</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  367</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Nord-Pas-de-Calais.html">France 3 Nord-Pas de Calais</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  368</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Paris-IdF.html">France 3 Paris IdF</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Paris-IdF.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  369</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Pays-de-la-Loire.html">France 3 Pays de la Loire</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Pays-Loire.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  370</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Picardie.html">France 3 Picardie</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><br></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  371</b></td>
<td align="center"><font face="Verdana" size=1>120</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Poitou-Charentes.html">France 3 Poitou-Charentes</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Poitou-Charentes.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>091228</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  372</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Provence-Alpes.html">France 3 Provence-Alpes</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Provence-Alpes.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121116</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  373</b></td>
<td align="center"><font face="Verdana" size=1>118</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-3.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france3.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-3-Rhone-Alpes.html">France 3 Rhône-Alpes</a></b></td>
<td bgcolor="#ffcc99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/df/France-3-Rhone-Alpes-Auvergne.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100118</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  400</b></td>
<td align="center"><font face="Verdana" size=1>20</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/qa/Al-Jazeera-Channel.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/aa/al_jazeera_channel_qa.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/Al-Jazeera-Channel.html">Al Jazeera Channel</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/qa/Al-Jazeera-Channel.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ab/Al-Jazeera-Sat-Ch.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/Qatar.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1KR-Europe.html">Europe</a><br>0-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100301</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  401</b></td>
<td align="center"><font face="Verdana" size=1>22</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/France-24-Arabic.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france24_arabic.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-24-Arabic.html">France 24 Arabic</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/fr/France-24-Arabic.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/France-24-Arabe.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1KR-Europe.html">Europe</a><br>0-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>T Viererbe<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  407</b></td>
<td align="center"><font face="Verdana" size=1>22</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/France-24-English.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france24_english.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-24-English.html">France 24 English</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/fr/France-24-English.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/France-24-English.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1KR-Europe.html">Europe</a><br>0-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>F Dibba<br>061207</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  408</b></td>
<td align="center"><font face="Verdana" size=1>20</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/qa/Al-Jazeera-English.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/aa/al_jazeera_english.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/Al-Jazeera-English.html">Al Jazeera English</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/qa/Al-Jazeera-English.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ab/Al-Jazeera-Eng.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/Qatar.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1KR-Europe.html">Europe</a><br>0-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>V Hrubec<br>061115</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  471</b></td>
<td align="center"><font face="Verdana" size=1>22</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/cctv_francais.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/cn/CCTV-Francais.html">CCTV Français</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/cn/CCTV-Francais.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/cc/CCTV-Francais.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/China.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1KR-Europe.html">Europe</a><br>0-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>100901</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  472</b></td>
<td align="center"><font face="Verdana" size=1>20</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/kr/Arirang-World.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/aa/arirang_world.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/kr/Arirang-World.html">Arirang World</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/kr/Arirang-World.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ab/Arirang-TV-World.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/South-Korea.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1KR-Europe.html">Europe</a><br>0-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>M Vyletal<br>060329</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  473</b></td>
<td align="center"><font face="Verdana" size=1>20</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/jp/NHK-World-TV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/nhk_world.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/jp/NHK-World-TV.html">NHK World TV</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/tvchannels/jp/NHK-World-TV.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ln/NHK-World-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/Japan.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1KR-Europe.html">Europe</a><br>0-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>Anonymous<br>081125</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  500</b></td>
<td align="center"><font face="Verdana" size=1>90</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/TF1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tf1.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/TF-1.html">TF 1</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/tt/TF1.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>Tonicastro<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  501</b></td>
<td align="center"><font face="Verdana" size=1>88</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/France-2.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ff/france2.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/France-2.html">France 2</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/df/France2.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140515</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  503</b></td>
<td align="center"><font face="Verdana" size=1>80</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_clair.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---France.html">Canal + France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Canal+.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  504</b></td>
<td align="center"><font face="Verdana" size=1>88</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/M6.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/m6_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/M6.html">M6</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/M6.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140515</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  505</b></td>
<td align="center"><font face="Verdana" size=1>88</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/ARTE-Francais.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/aa/arte_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/ARTE-Francais.html">ARTE Français</a></b></td>
<td bgcolor="#99ff99"><img src="../t.gif" title="Teletext" height=15 width=15 align=absbottom border=0><a href="http://www.lyngsat-address.com/ab/ARTE-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140515</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  506</b></td>
<td align="center"><font face="Verdana" size=1>80</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---Cinema-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cinema.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---Cinema-France.html">Canal + Cinéma France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Canal+Cinema.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  507</b></td>
<td align="center"><font face="Verdana" size=1>80</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---Sport-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_sport.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---Sport-France.html">Canal + Sport France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Canal+Sport.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  508</b></td>
<td align="center"><font face="Verdana" size=1>80</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---Family-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_family.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---Family-France.html">Canal + Family France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Canal+Family.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  509</b></td>
<td align="center"><font face="Verdana" size=1>80</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Canal---Decale.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_decale.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Canal---Decale.html">Canal + Décalé</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Canal+Decale.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  512</b></td>
<td align="center"><font face="Verdana" size=1>88</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine---Premier.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cine_plus_premier.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cine---Premier.html">Cine + Premier</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Cine+Premier.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140515</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  513</b></td>
<td align="center"><font face="Verdana" size=1>80</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine---Frisson.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cine_plus_frisson.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cine---Frisson.html">Cine + Frisson</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Cine+Frisson.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121114</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  514</b></td>
<td align="center"><font face="Verdana" size=1>88</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cine---Emotion.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_cine_plus_emotion.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cine---Emotion.html">Cine + Émotion</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Cine+Emotion.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140515</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  518</b></td>
<td align="center"><font face="Verdana" size=1>102</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/OCS-Max.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/oo/ocs_max.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/OCS-max.html">OCS max</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/or/Orange-cinemax.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>120405</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  519</b></td>
<td align="center"><font face="Verdana" size=1>86</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/TCM-Cinema.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tcm_cinema_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/TCM-Cinema.html">TCM Cinéma</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/tt/TCM-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br>Viaccess 5.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  520</b></td>
<td align="center"><font face="Verdana" size=1>70</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/OCS-Choc.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/oo/ocs_choc.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/OCS-choc.html">OCS choc</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/or/Orange-cinechoc.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  521</b></td>
<td align="center"><font face="Verdana" size=1>84</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/OCS-City.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/oo/ocs_city.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/OCS-City.html">OCS City</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/or/Orange-City-HBO.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe.html">Europe</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>Anonymous<br>131011</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  522</b></td>
<td align="center"><font face="Verdana" size=1>92</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/OCS-Geants.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/oo/ocs_geants.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/OCS-geants.html">OCS geants</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/or/Orange-cinegeants.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  523</b></td>
<td align="center"><font face="Verdana" size=1>84</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Disney-Cinemagic-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/disney_cinemagic_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Disney-Cinemagic-France.html">Disney Cinemagic France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/df/Disney-Cinemagic-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe.html">Europe</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130910</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  524</b></td>
<td align="center"><font face="Verdana" size=1>92</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/13eme-Rue.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/num/13eme_rue_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/13eme-Rue.html">13ème Rue</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/uz/13emeRue-CSN.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>T Viererbe<br>120626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  525</b></td>
<td align="center"><font face="Verdana" size=1>92</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Syfy-Universal-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ss/syfy_universal_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Syfy-France.html">Syfy France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ss/Syfy-Uni-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>T Viererbe<br>120626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  526</b></td>
<td align="center"><font face="Verdana" size=1>102</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/TF6.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tf6.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/TF-6.html">TF 6</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/tt/TF6.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>120124</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  527</b></td>
<td align="center"><font face="Verdana" size=1>102</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Serie-Club.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ss/serie_club.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Serie-Club.html">Série Club</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ss/Serie-Club-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>120124</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  529</b></td>
<td align="center"><font face="Verdana" size=1>86</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Teva.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/teva_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Teva.html">Téva</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/tt/Teva.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130910</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  530</b></td>
<td align="center"><font face="Verdana" size=1>90</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Paris-Premiere.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/paris_premiere.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Paris-Premiere.html">Paris Première</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/or/Paris-Premiere.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>T Viererbe<br>120626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  531</b></td>
<td align="center"><font face="Verdana" size=1>70</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/au/MTV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mtv_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/MTV-France.html">MTV France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/MTV-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  532</b></td>
<td align="center"><font face="Verdana" size=1>88</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/W9.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ww/w9_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/W9.html">W9</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/uz/W9.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140515</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  543</b></td>
<td align="center"><font face="Verdana" size=1>68</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Cherie-25.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/cherie_25_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Cherie-25.html">Chérie 25</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Cherie-25-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>C Cornillet<br>130304</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  546</b></td>
<td align="center"><font face="Verdana" size=1>90</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Planete--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/planete_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Planete--.html">Planète +</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/or/Planete-CSN.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  547</b></td>
<td align="center"><font face="Verdana" size=1>70</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Planete---Thalassa.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/pp/planete_plus_thalassa.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Planete---Thalassa.html">Planète + Thalassa</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/or/Planete-Thalassa.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  550</b></td>
<td align="center"><font face="Verdana" size=1>86</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Discovery-HD-Showcase.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/discovery_hd_showcase_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Discovery-HD-Showcase.html">Discovery HD Showcase</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/df/Discovery-HD-Showcase.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  551</b></td>
<td align="center"><font face="Verdana" size=1>84</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Discovery-Science.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/discovery_science_global.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Discovery-Science-Channel.html">Discovery Science Channel</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/tt/The-Science-Channel.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe.html">Europe</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121114</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  552</b></td>
<td align="center"><font face="Verdana" size=1>90</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/National-Geographic-Channel.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/nn/nat_geo_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/National-Geographic-Channel-France.html">National Geographic Channel France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/NGC-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>Tonicastro<br>101012</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  555</b></td>
<td align="center"><font face="Verdana" size=1>88</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Ushuaia-TV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/uu/ushuaia_tv.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Ushuaia-TV.html">Ushuaïa TV</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/uz/Ushuaia-TV.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140515</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  562</b></td>
<td align="center"><font face="Verdana" size=1>90</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/at/EuroSport.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/eurosport.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Eurosport.html">Eurosport</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/df/Eurosport.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>T Viererbe<br>120626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  563</b></td>
<td align="center"><font face="Verdana" size=1>84</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/be/EuroSport-2.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/ee/eurosport2.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Eurosport-2.html">Eurosport 2</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/df/Eurosport-2.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe.html">Europe</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121114</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  564</b></td>
<td align="center"><font face="Verdana" size=1>84</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Ma-Chaine-Sport.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/ma_chaine_sport.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Ma-Chaine-Sport.html">Ma Chaîne Sport</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/Ma-Chaine-Sport.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe.html">Europe</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>121114</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  565</b></td>
<td align="center"><font face="Verdana" size=1>102</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Kombat-Sport.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/kk/kombat_sport.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/lu/Kombat-Sport.html">Kombat Sport</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/gk/Kombat-Sport.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>120919</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  570</b></td>
<td align="center"><font face="Verdana" size=1>12</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/qa/BeIn-Sports-1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_1.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-1.html">BeIn Sports 1</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-1.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>S Günes<br>120605</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  571</b></td>
<td align="center"><font face="Verdana" size=1>12</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/qa/BeIn-Sports-2.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/bb/be_in_sports_2.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/qa/BeIn-Sports-2.html">BeIn Sports 2</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ab/BEIN-Sport-2.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>F Dibba<br>120809</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  572</b></td>
<td align="center"><font face="Verdana" size=1>86</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Golf--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_golf_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Golf--.html">Golf +</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/CanalSat-Golf+.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  578</b></td>
<td align="center"><font face="Verdana" size=1>90</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Disney-Channel.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/disney_channel_global.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Disney-Channel-France.html">Disney Channel France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/df/Disney-Channel-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>T Viererbe<br>120626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  579</b></td>
<td align="center"><font face="Verdana" size=1>84</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Disney-XD.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/disney_xd_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Disney-XD-France.html">Disney XD France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/df/Disney-XD-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe.html">Europe</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>130910</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  580</b></td>
<td align="center"><font face="Verdana" size=1>92</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/ae/Disney-Junior.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/disney_junior_us.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/Disney-Junior-France.html">Disney Junior France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/df/Disney-Junior-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>E Pellissier<br>T Viererbe<br>120626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  585</b></td>
<td align="center"><font face="Verdana" size=1>70</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MTV-Base.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mtv_base_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/MTV-Base-France.html">MTV Base France</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/MTV-Base-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  586</b></td>
<td align="center"><font face="Verdana" size=1>70</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MTV-Pulse.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mtv_pulse.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/MTV-Pulse.html">MTV Pulse</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/MTV-Pulse-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  587</b></td>
<td align="center"><font face="Verdana" size=1>70</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MTV-Idol.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/mtv_idol.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/us/MTV-Idol.html">MTV Idol</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/MTV-Idol.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  591</b></td>
<td align="center"><font face="Verdana" size=1>102</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/M6-Music.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/m6_music_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/M6-Music.html">M6 Music</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/M6-Music-Hits.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>P Piotrowski<br>110920</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  593</b></td>
<td align="center"><font face="Verdana" size=1>90</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/DJazz-TV.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/d_jazz_tv_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/nl/Djazz-TV.html">Djazz TV</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/df/Djazz-TV-NL.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>D Shimoni<br>130409</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b><br></b></td>
<td align="center"><font face="Verdana" size=1>18</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/CanalSat-France-Promo.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalsat_fr_promo.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b>CanalSat France promo</b></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>090521</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b><br></b></td>
<td align="center"><font face="Verdana" size=1>92</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Voyage.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/vv/voyage.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Voyage.html">Voyage</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/uz/Voyage.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>130626</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b><br></b></td>
<td align="center"><font face="Verdana" size=1>92</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/MCS-Extreme.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/mm/ma_chaine_sport_extreme.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/MCS-Extreme.html">MCS Extrême</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/ln/MCS-Extreme.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>T Viererbe<br>131113</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b><br></b></td>
<td align="center"><font face="Verdana" size=1>70</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Comedie--.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_comedie_plus.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Comedie--.html">Comédie +</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/cc/Comedie.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>131112</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b><br></b></td>
<td align="center"><font face="Verdana" size=1>70</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/Jimmy.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/jj/jimmy_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#99ff99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Jimmy.html">Jimmy</a></b></td>
<td bgcolor="#99ff99"><a href="http://www.lyngsat-address.com/gk/Jimmy-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#99ff99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br> MPEG-4/HD</td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>N Schlammer<br>140212</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b><br></b></td>
<td align="center"><font face="Verdana" size=1>76</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tvchannel/fr/D-17.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/cc/canalplus_fr_d17.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/D17.html">D17</a></b></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/cc/Canal+D17.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1M-Europe-Wide.html">Europe Wide</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140131</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b><br></b></td>
<td align="center"><font face="Verdana" size=1>96</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/d_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/dd/dorcel_xxx.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffcc99"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/nl/Dorcel-XXX.html">Dorcel XXX</a></b></td>
<td bgcolor="#ffcc99"><a href="http://www.lyngsat-address.com/df/Dorcel-XXX.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffcc99"><font face="Verdana" size=1>Mediaguard 2<br>Nagravision 3<br>Viaccess 3.0<br>Viaccess 4.0<br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140513</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b><br></b></td>
<td align="center"><font face="Verdana" size=1>100</td>
<td align="center"><br><br>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2 color="gray"><b>[CanalSat France info card]</b></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140526</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b><br></b></td>
<td align="center"><font face="Verdana" size=1>106</td>
<td align="center"><br><br>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2 color="gray"><b>[CanalSat info card]</b></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>140513</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b><br></b></td>
<td align="center"><font face="Verdana" size=1>110</td>
<td align="center"><a href="http://www.lyngsat-logo.com/tv/t_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/tv/tt/tek_tv_shop_fr.png" width=52 height=39 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=2><b><a href="http://www.lyngsat.com/tvchannels/fr/Tek-TV-Shop.html">Tek TV Shop</a></b> </font><font size=1>(08-11)</td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/tt/Tek-TV-Shop.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freetv/France.html"><img src="../f.gif" title="LyngSat Free TV" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1N-Pan-Europe.html">Pan Europe</a><br><font color="green"></font></td>
<td><font face="Verdana" size=1>R László 2<br>131230</td>
</tr>
<tr>
<td colspan=8 align="center"><font face="Arial" size=2>CanalSat France &#169; LyngSat, last updated 2014-07-20 - http://www.lyngsat.com/packages/CanalSat-France_chno.html</td>
</tr>
</table>
<p>

<table width=720 border cellspacing=0 cellpadding=0>
<tr>
<td colspan=8 align="center"><font face="Arial" size=2>CanalSat France &#169; LyngSat, last updated 2014-07-20 - http://www.lyngsat.com/packages/CanalSat-France_chno.html</td>
</tr>
<tr>
<td width=70 align="center"><font face="Verdana" size=2>Ch</td>
<td width=50 align="center"><font face="Verdana" size=2>Tp</td>
<td width=39 align="center"><br></td>
<td width=220 align="left"><font face="Verdana" size=2>Channel Name</td>
<td width=15><br></td>
<td width=90 align="left"><font face="Verdana" size=1><font size=1>Enc. system</td>
<td width=50 align="left"><font face="Verdana" size=1>Beam<br>EIRP&nbsp;(dBW)<br><font color="green">C/N lock</font></td>
<td width=80 align="left"><font face="Verdana" size=1>Source<br>Updated</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  199</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><br>
</td>
<td bgcolor="#ffaaff"><font face="Arial"><font size=1><b>@ </font><font size=2><a href="http://www.canalsat.fr/index.php?cid=53092&rhl=9&rhl=9">CanalSat Radios 1</a></b></td>
<td bgcolor="#ffaaff"><br></td>
<td bgcolor="#ffaaff"><font face="Verdana" size=1><br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>L Martin<br>080126</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  199</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><br>
</td>
<td bgcolor="#ffaaff"><font face="Arial"><font size=1><b>@ </font><font size=2><a href="http://www.canalsat.fr/index.php?cid=53092&rhl=9&rhl=9">CanalSat Radios 2</a></b></td>
<td bgcolor="#ffaaff"><br></td>
<td bgcolor="#ffaaff"><font face="Verdana" size=1><br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>L Martin<br>080126</td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  199</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><br>
</td>
<td bgcolor="#ffaaff"><font face="Arial"><font size=1><b>@ </font><font size=2><a href="http://www.canalsat.fr/index.php?cid=53092&rhl=9&rhl=9">CanalSat Radios 3</a></b></td>
<td bgcolor="#ffaaff"><br></td>
<td bgcolor="#ffaaff"><font face="Verdana" size=1><br></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1>L Martin<br>080126</td>
</tr>
<tr>
<td colspan=8 align="center"><font face="Arial" size=2>CanalSat France &#169; LyngSat, last updated 2014-07-20 - http://www.lyngsat.com/packages/CanalSat-France_chno.html</td>
</tr>
</table>
<p>

<table width=720 border cellspacing=0 cellpadding=0>
<tr>
<td colspan=8 align="center"><font face="Arial" size=2>CanalSat France &#169; LyngSat, last updated 2014-07-20 - http://www.lyngsat.com/packages/CanalSat-France_chno.html</td>
</tr>
<tr>
<td width=70 align="center"><font face="Verdana" size=2>Ch</td>
<td width=50 align="center"><font face="Verdana" size=2>Tp</td>
<td width=39 align="center"><br></td>
<td width=220 align="left"><font face="Verdana" size=2>Channel Name</td>
<td width=15><br></td>
<td width=90 align="left"><font face="Verdana" size=1><font size=1>Enc. system</td>
<td width=50 align="left"><font face="Verdana" size=1>Beam<br>EIRP&nbsp;(dBW)<br><font color="green">C/N lock</font></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  450</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/France-Culture.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ff/france_culture.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/France-Culture.html">France Culture</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/France-Culture.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/France-Culture.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  451</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/f_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ff/france_musique.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/France-Musique.html">France Musique</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/France-Musique.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/France-Musique.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  452</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/FIP.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ff/france_fip.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/FIP.html">FIP</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/FIP.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/FIP.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  453</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/f_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ff/france_info.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/France-Info.html">France Info</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/France-Info.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/France-Info.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  454</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/France-Inter.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ff/france_inter.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/France-Inter.html">France Inter</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/France-Inter.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/France-Inter.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  455</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/France-Bleu.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ff/france_bleu.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/France-Bleu-1071.html">France Bleu 107.1</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/France-Bleu-1071.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/France-Bleu-Ille.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  456</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/f_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ff/france_le_mouv.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Le-Mouv.html">Le Mouv'</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Le-Mouv.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ln/Le-Mouv.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  457</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/r_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/rmc_doualiya.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Monte-Carlo-Doualiya.html">Monte Carlo Doualiya</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Monte-Carlo-Doualiya.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ln/Monte-Carlo-Doualiya.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  458</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Radio-France-Internationale.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/rfi_international.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Radio-France-Internationale.html">Radio France Internationale</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/or/RFI-Radio-France-International.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  459</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/RFI.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/rfi_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/RFI-Multilingues.html">RFI Multilingues</a></td>
<td bgcolor="#ffffbb"><br></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  460</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Europe-1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ee/europe1.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Europe-1.html">Europe 1</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Europe-1.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/Europe-1.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  461</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Virgin-Radio-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/vv/virgin_radio_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/uk/Virgin-Radio-France.html">Virgin Radio France</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/uk/Virgin-Radio-France.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/uz/Virgin-Radio-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/United-Kingdom.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  462</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/RFM.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/rfm_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/RFM.html">RFM</a> </font><font size=1>(France)</td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/RFM.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/RFM-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  463</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/RTL-Radio.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/rtl_radio.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/RTL.html">RTL</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/RTL.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/RTL.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  464</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/RTL-2.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/rtl2_radio_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/RTL-2.html">RTL 2</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/RTL-2.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/RTL2-Radio.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  465</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Fun-Radio.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ff/fun_radio.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Fun-Radio.html">Fun Radio</a> </font><font size=1>(France)</td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Fun-Radio.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/df/Fun-Radio.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  467</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/NRJ-France.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/nn/nrj_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/NRJ-France.html">NRJ France</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/NRJ-France.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ln/NRJ-France.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  468</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Cherie-FM.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/cc/cherie_fm.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Cherie-FM.html">Chérie FM</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Cherie-FM.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/cc/CherieFM.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  469</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Rire-et-Chansons.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/rire_chansons.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Rire-and-Chansons.html">Rire & Chansons</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Rire-and-Chansons.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Rire-et-Chansons.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  470</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Nostalgie-la-Legende.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/nn/nostalgie_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Nostalgie-la-legende.html">Nostalgie la légende</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Nostalgie-la-legende.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Radio-Nostalgie.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  471</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/r_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/rmc.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/RMC.html">RMC</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/RMC.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/RMC-Info.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  472</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/b_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/bb/bfm.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/BFM-Radio.html">BFM Radio</td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/ab/BFM-Radio-FR.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  473</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Sud-Radio.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ss/sud_radio_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Sud-Radio.html">Sud Radio</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Sud-Radio.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ss/Sud-Radio.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  474</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/t_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/tt/tsf_jazz.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/TSF-Jazz.html">TSF Jazz</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/TSF-Jazz.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/tt/TSF-Jazz.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  475</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/r_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/radio_nova.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Radio-Nova.html">Radio Nova</a> </font><font size=1>(France)</td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Radio-Nova.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Radio-Nova.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  476</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Radio-FG.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/radio_fg_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/FG.html">FG</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/FG.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Radio-FG.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  478</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/c_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/cc/contact_fm.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Contact.html">Contact</td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/cc/Contact-FM.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  479</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Latina.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ll/latina_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Latina.html">Latina</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Latina.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Radio-Latina.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  480</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/j_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/jj/jazz_radio_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Jazz-Radio.html">Jazz Radio</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Jazz-Radio.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  481</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/s_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ss/skyrock.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Skyrock.html">Skyrock</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Skyrock.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ss/Skyrock.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  482</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/r_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/radio_classique.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Radio-Classique.html">Radio Classique</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Radio-Classique.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Radio-Classique.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  483</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/v_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/vv/voltage.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Voltage.html">Voltage</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Voltage.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/uz/Voltage.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  484</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Oui-FM.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/oo/oui_fm_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Oui-FM.html">Oüi FM</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Oui-FM.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Oui-FM.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  487</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Radio-Notre-Dame.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/radio_notre_dame_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Radio-Notre-Dame.html">Radio Notre Dame</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Radio-Notre-Dame.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Radio-Notre-Dame.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  488</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Radio-Alfa.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/radio_alfa_fr.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Radio-Alfa.html">Radio Alfa</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Radio-Alfa.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Radio-Alfa.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  489</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/r_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/rcj.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/RCJ.html">RCJ</a> </font><font size=1>(08.00-08.30 & 11.00-14.00 & 23.00-24.00)</td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/RCJ.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/RCJ-Juive.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  489</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/s_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/ss/shalom_radio.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Radio-Shalom.html">Radio Shalom </font><font size=1>(16.30-21.00)</td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/or/Radio-Shalom.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  490</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/fr/Beur-FM.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/bb/beur_fm.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Beur-FM.html">Beur FM</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Beur-FM.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/ab/Beur-FM.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  493</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/uk/BBC-World-Service.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/bb/bbc_world_service_uk.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/uk/BBC-World-Service-English.html">BBC World Service English</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/ab/BBC-World-Service.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/United-Kingdom.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  494</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radiochannel/uk/BBC-World-Service.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/bb/bbc_world_service_uk.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/uk/BBC-World-Service-Arabic.html">BBC World Service Arabic</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-address.com/ab/BBC-World-Service.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/United-Kingdom.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td align="center"><font face="Verdana" size=2><b>  496</b></td>
<td align="center"><font face="Verdana" size=1>24</td>
<td align="center"><a href="http://www.lyngsat-logo.com/radio/r_1.html" title="LyngSat Logo"><img src="http://www.lyngsat-logo.com/logo/radio/rr/radio_courtoisie.png" width=28 height=21 border=0></a>
</td>
<td bgcolor="#ffffbb"><font face="Arial"><font size=1><a href="http://www.lyngsat.com/radiochannels/fr/Radio-Courtoisie.html">Radio Courtoisie</a></td>
<td bgcolor="#ffffbb"><a href="http://www.lyngsat-stream.com/radiochannels/fr/Radio-Courtoisie.html"><img src="../s.gif" title="LyngSat Stream" height=15 width=15 align=absbottom border=1></a><a href="http://www.lyngsat-address.com/or/Radio-Courtoisie.html"><img src="../a.gif" title="LyngSat Address" height=15 width=15 align=absbottom border=1></a></td>
<td bgcolor="#ffffbb"><font face="Verdana" size=1><a href="http://www.lyngsat.com/freeradio/France.html"><img src="../f.gif" alt="LyngSat Free Radio" height=15 width=15 align=absbottom border=1></a></td>
<td><font face="Verdana" size=1><a href="http://www.lyngsat-maps.com/footprints/Astra-1L-Europe-Ku.html">Europe Ku</a><br>49-51<br><font color="green"></font></td>
<td><font face="Verdana" size=1><br></td>
</tr>
<tr>
<td colspan=8 align="center"><font face="Arial" size=2>CanalSat France &#169; LyngSat, last updated 2014-07-20 - http://www.lyngsat.com/packages/CanalSat-France_chno.html</td>
</tr>
</table>
<p>

<font face="Arial" size=2></font>
<p>

<table align="center">
<tr>
<td><font face="Arial" size=1>Colour codes on this package chart:</td>
<td bgcolor=#ffffbb><font face="Arial" size=1>SD/clear</td>
<td bgcolor=#ffcc99><font face="Arial" size=1>SD/encrypted</td>
<td bgcolor=#ddffdd><font face="Arial" size=1>HD/clear</td>
<td bgcolor=#99ff99><font face="Arial" size=1>HD/encrypted</td>
<td bgcolor=#ffaaff><font face="Arial" size=1>interactive</td>
</tr>
</table>

<p>
<p>

<table width=720 border=0 cellspacing=0 cellpadding=0>
<tr>
<td><img src="http://www.lyngsat.com/images/pixel_brun.gif" width=720 height=1 vspace=7></td>
</tr>
<tr align="center">
<td>
<table border=0 cellspacing=0 cellpadding=0>
<tr align="center">
<td align="center">
<table width=728 border=0 cellspacing=1 cellpadding=1><tr><td align='center' width=728 height=90><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- LyngSat 728x90 #2 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-9851824455959986"
     data-ad-slot="8866439388"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></td>
</tr></table><p>

  <table width=468 height=60 cellspacing=0 cellpadding=0 bordercolor="black" border=0>
  <tr>
  <td align="center" bordercolor="moccasin" bgcolor="moccasin"><font face="Arial" size=2>
  The content on this site is protected by copyright. All rights reserved.  <br>
  LyngSat is a registered trademark, owned by Lyngemark Satellite.  <br>
  © Lyngemark Satellite  </td>
  </tr>
  </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table width=720 border=0 cellspacing=0 cellpadding=0>
<tr>
<td align="center">
<p>

<font face="Arial" size=2>You are welcome to send updates and corrections to <b><a href="mailto:webmaster@lyngsat.com">webmaster@lyngsat.com</a></b><br>
Logotypes provided by <a href="http://www.lyngsat-logo.com/">LyngSat Logo</a></font>
<p>

</td>
</tr>
</table>
<td width="160" align="center" valign="top">
<table border=0 cellspacing=1 cellpadding=1>
<tr><td align='center'><font face='Arial' size=2><a href='http://www.lyngsat.com/advert.html'><font color='blue'><b>Advertising</b></font></a></font></td></tr><tr align='center'>
<td width=160 height=600><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- LyngSat 160x600 #2 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:160px;height:600px"
     data-ad-client="ca-pub-9851824455959986"
     data-ad-slot="1509965252"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></td>
</tr>
<tr align='center'>
<td width=160 height=160><a href='http://www.lyngsat.info/adverts/redirect.php?url=http://www.m3sat.com/&pg=www.lyngsat.com/packages/CanalSat-France_chno.html&im=m3sat_160_hb.gif&pl=R600' target='advertwin'><img src='../adverts/images/m3sat_160_hb.gif' width=160 height=160 border=0></a></td>
</tr>
<tr align='center'>
<td width=160 height=80><a href='http://www.lyngsat.info/adverts/redirect.php?url=http://www.promaxelectronics.com/ing/products/tv-cable-and-satellite-field-strength-meters/HDRanger2/touch-screen-tv-and-satellite-analyser/?utm_source=lyngsat.com&utm_medium=banner&utm_campaign=hdranger2_en&pg=www.lyngsat.com/packages/CanalSat-France_chno.html&im=promax_hd-ranger-2.gif&pl=R760' target='advertwin'><img src='../adverts/images/promax_hd-ranger-2.gif' width=160 height=80 border=0></a></td>
</tr>
<tr align='center'>
<td width=160 height=80><a href='http://www.lyngsat.info/adverts/redirect.php?url=http://www.lyngsat.info/adverts/rrsat_general.html&pg=www.lyngsat.com/packages/CanalSat-France_chno.html&im=rrsat_general_1401.gif&pl=R840' target='advertwin'><img src='../adverts/images/rrsat_general_1401.gif' width=160 height=80 border=0></a></td>
</tr>
</table>
</td>
</tr>
</table>
<!-- big table end -->
</div>
</body>
</html>
