#!/usr/bin/env perl
use strict; 
use warnings;
use HTML::TreeBuilder::XPath;
use Data::Dumper;


# configurer le fichier de sauvegarde
	my $home = $ENV{HOME};
	$home = '.' if not defined $home;
	my $conf_dir = "$home";
	(-d $conf_dir) or mkdir($conf_dir, 0777)
	or die "cannot mkdir $conf_dir: $!";
	my $config_file ="pack_canalsat_option.txt";
	

#calcul de date
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $date =  $mday."/".($mon+1)."/".(1900+$year);# recuperation des données	
	my $url = ("www.canalsat.fr/pid2270-comprendre-pack-panorama.html");
#	my $tree = HTML::TreeBuilder::XPath->new_from_url($url);
my $html = do {local $/; <DATA>};
my $tree = HTML::TreeBuilder::XPath->new();
$tree->parse( $html );
	
# ouvrir le fichier de sauvegarde
	open(CONF, "> $config_file") or die "Cannot write to $config_file: $!";
	print CONF"
# liste des chaines en option d'abonnement pour canalsat

# structure du fichier
# fournisseur, bouquet, genre, nom de chaine
#
# crée le $date par Gilles Choteau à partir de la page 
#  $url
#
";
my @theme;
my @elements = map { $_->parent } 
	  @{ $tree->findnodes('.//div[@class="liste-chaines"]')};
#print Dumper @elements;	  
	  
	foreach  my $element (@elements) {
		
		foreach  my $th ($element-> findnodes('//div[@class="liste-thematiques"]/ul/li')) {
			my $number = $th ->findnodes('.//@data-id');
			$theme[$number] = $th ->findnodes('.//span') if $number;
		}
		foreach  my $el ($element-> findnodes('.//div[@class="chaine-detail"]')) {
		my $bouquet = $el ->findnodes('.//@data-offres');
		my $theme = $el ->findnodes('.//@data-thematiques');
		$theme =~s/\[-1,//;
		$theme =~s/\]//;
		$theme = $theme[$theme];
		my $chaine = $el ->findnodes('.//@title');
		$chaine =~ s/\s-\schaine\sen\soption//;
#		$chaine =~m/(^\w+$)\s/;
		$bouquet =~s/\[//;
		$bouquet =~s/\]//;
		$bouquet =~ s/\s+//g;
		
		
		
		
#		print "bouquet = $bouquet\t $theme\t $chaine\n" if !$bouquet ;



				print CONF "canalsat,option,$theme,$chaine\n" if !$bouquet;
	}}
	
#		foreach my $key(keys@theme){
#			print "$key\t $theme[$key]\n" if $theme[$key] ;
#		}

	print "Fichier sauvegardé sous $config_file\n";	
	

#print Dumper @elements;
#print Dumper @results;


__DATA__


<!DOCTYPE html><html><head><title>CanalSat - Découvrir toutes les chaines des offres CANALSAT</title>
<meta content="IE=Edge" http-equiv="X-UA-Compatible" />
<meta content="index, follow" name="robots" />
<meta content="2 days" name="revisit-after" />
<meta content="fr" http-equiv="content-language" />
<meta content="text/html; charset=ISO-8859-1" http-equiv="Content-Type" />
<meta content="text/javascript; charset=ISO-8859-1" http-equiv="Content-Script-Type" />
<meta content="text/css; charset=ISO-8859-1" http-equiv="Content-Style-Type" />
<meta content="width=1000, user-scalable=yes, maximum-scale=1.0, target-densitydpi = device-dpi" name="viewport" />
<meta content="NOIMAGEINDEX" name="robot" />
<meta content="CanalSat" name="Author" />

<link href="/fonts/fonts.css" media="screen, print" rel="stylesheet" type="text/css" />
<link href="http://newmedia.canal-plus.com/design_pack/front_office_canalsat_v4/css/header.1805c3a1398c97038d295b5f83b49c0e.css" rel="stylesheet" type="text/css" />
<link href="https://plus.google.com/102469243337710478299" rel="publisher" />

<script src="http://assets.adobedtm.com/75b97122605cbde107b38740210932f5c290a210/satelliteLib-fd97e0bfc398fc62f3585d052be63f0af1f7c1b7.js" type="text/javascript"></script>
<script src="http://newmedia.canal-plus.com/design_pack/front_office_canalsat_v4/js/header.8a6e0c6fe2aa7047c2e242fd1587565d.js" type="text/javascript"></script>
<script src="http://newmedia.canal-plus.com/design/front_office_canalsat_v4/js/cufon2.js" type="text/javascript"></script>
<script src="http://newmedia.canal-plus.com/design/front_office_canalsat_v4/js/cufon2.Canal_400.font.js" type="text/javascript"></script>
<script src="http://newmedia.canal-plus.com/design/front_office_canalsat_v4/js/cufon2.CanalDem_400.font.js" type="text/javascript"></script>
<script src="http://newmedia.canal-plus.com/design/front_office_canalsat_v4/js/cufon2.Canal_italic_400.font.js" type="text/javascript"></script>
<script src="http://newmedia.canal-plus.com/design/front_office_canalsat_v4/js/cufon2.Canal_italic_700.font.js" type="text/javascript"></script>
<script src="http://newmedia.canal-plus.com/design/front_office_canalsat_v4/js/cufon2.CanalBoldRomain_400.font.js" type="text/javascript"></script>
<script src="http://newmedia.canal-plus.com/design/front_office_canalsat_v4/js/ie_jquery_document_size.js" type="text/javascript"></script>
<script src="http://player.canalplus.fr/common/js/canalPlayer.js?param=csat_programme" type="text/javascript"></script>
<script type="text/javascript">  function passAuthSuccessCallback() {
						if(passJSON.auth_level == 3 || passJSON.auth_level == 0) {
						document.cookie = "profil=; path=/;domain=.canalsat.fr; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
						}
						}
playerEmbarqueWidth = '';
playerEmbarqueHeight = '';
playerEmbarqueUrl = 'http://player.canalplus.fr/site/flash/player.swf';
playerEmbarqueInstallUrl = 'http://canal5.dev.canalsat.beta/flash/install/expressinstall.swf';
playerEmbarqueVars.param = 'csat';
playerEmbarqueVars.autoplay = '';
var sUrlBigPlayer = 'http://www.canalsat.fr/player';
var sRequestURI = '@pid2280-toutes-les-chaines.html';
var sThematiqueListe = '';
var _idpageCible = '1011';
var pageGrille = '/pid1002-grille-tv.html';
var sImageFront = 'http://newmedia.canal-plus.com';
var habillage = false;
WebTvError = '';
var iEADErrorCode = -1
var programme = '';
var rec = '';
var dfid = '';
var chaineId = '';
var iTemplatePage = '398';</script>


<script type="text/javascript" charset="UTF-8">
/* <![CDATA[ */
try { if (undefined == xajax.config) xajax.config = {}; } catch (e) { xajax = {}; xajax.config = {}; };
xajax.config.requestURI = "/lib/index_tools/xajax.server.php";
xajax.config.statusMessages = false;
xajax.config.waitCursor = true;
xajax.config.version = "xajax 0.5 Beta 4";
xajax.config.legacy = false;
xajax.config.defaultMode = "asynchronous";
xajax.config.defaultMethod = "POST";
/* ]]> */
</script>
<script type="text/javascript" src="http://newmedia.canal-plus.com/lib/other_tools/xajax/xajax_js/xajax_core.js" charset="UTF-8"></script>
<script type="text/javascript" charset="UTF-8">
/* <![CDATA[ */
window.setTimeout(
 function() {
  var scriptExists = false;
  try { if (xajax.isLoaded) scriptExists = true; }
  catch (e) {}
  if (!scriptExists) {
   alert("Error: the xajax Javascript component could not be included. Perhaps the URL is incorrect?\nURL: http://newmedia.canal-plus.com/lib/other_tools/xajax/xajax_js/xajax_core.js");
  }
 }, 2000);
/* ]]> */
</script>

<script type='text/javascript' charset='UTF-8'>
/* <![CDATA[ */
xajax_call = function() { return xajax.request( { xjxfun: 'call' }, { parameters: arguments } ); };
/* ]]> */
</script>
<script type="text/javascript">function xajax_loading(id){document.getElementById(id).innerHTML = '&lt;img alt=&quot;Chargement...&quot; border=&quot;0&quot; src=&quot;/lib/images/ajax/ajax-loader.gif&quot; /&gt; Chargement...';}</script><script type="text/javascript">function xajax_callWithNewURI () { 
                        temp = xajax.config.requestURI;
                        temp2 = xajax.config.defaultMethod;
                        xajax.config.defaultMethod = 'GET';
                        xajax.config.requestURI = '/ajax/'+arguments[0]+'/server.php';
                        xajax_call.apply(null, arguments);
                        xajax.config.requestURI = temp;
                        xajax.config.defaultMethod = temp2;
                    }
                </script>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /><meta http-equiv="pragma" content="no-cache" /><!--[if IE 7]><link rel="stylesheet" type="text/css" href="http://newmedia.canal-plus.com/design/front_office_canalsat_v4/css/ie7.css" /><![endif]--><!--[if IE 8]><link rel="stylesheet" type="text/css" href="http://newmedia.canal-plus.com/design/front_office_canalsat_v4/css/ie7.css" /><![endif]-->
<!--cpldyn-v01-->
</head><body>
<script>var passOmnitureChannel='CANALSAT.FR';</script>
<script src='https://media-pass.canal-plus.com/latest/js/bundle.js'></script>
<link href='https://media-pass.canal-plus.com/1/css/basic.css' rel='stylesheet' />
<script src='https://media-pass.canal-plus.com/latest/js/sso/cookies-utils.js'></script>
<div id="wrap" class="pointer">
<!-- Zone /page/commun_v5/header.php-->
<div id="header" >
	<div data-feedback="true" class="wrapper" id="header_wrapper">
		<div data-url="http://www.canalsat.fr" id="inf-header">
			<div id="top-header">
				<div id="top-logo" class="h1" >					<a title="CANALSAT" class="no-background-css" href="/"><img src="http://newmedia.canal-plus.com/image/23/8/103238.png" title="CANALSAT" alt="CANALSAT" /></a>
				</div>				<ul id="user" class="headerv5" style="display: block;"></ul>
				<div class="pass_hello" style="display:none;"></div>			</div>
		</div>
		<nav id="nav" data-action="pulldown">
			<ul id="navigation_primaire" class="">
												 					 					 												<li class="nav-headerv5 no-link first" data-action="pulldown-item" data-height="131">
						<a  href="javascript:void(0);"  						 title="Découvrir" class="majuscules omniture sans-over" >
							<h4 class="">
								<span>
									Découvrir
								</span>
							</h4>
						</a>
                                                <nav class="nav-4items multi">
                            <ul>
                                                                <li data-color="#0083c5" class="simple"><a title="Cinéma" href="/pid2261-decouvrir-cinema.html" >Cinéma</a></li>
                                                                <li data-color="#004494" class="simple"><a title="Séries" href="/pid2262-decouvrir-serie.html" >Séries</a></li>
                                                                <li data-color="#682081" class="simple"><a title="Jeunesse" href="/pid2260-decouvrir-jeunesse.html" >Jeunesse</a></li>
                                                                <li data-color="#ad086c" class="simple last"><a title="Campus" href="/pid2269-decouvrir-campus.html" >Campus</a></li>
                                                                <li data-color="#0083c5" class="simple"><a title="Découverte" href="/pid2265-decouvrir-decouverte.html" >Découverte</a></li>
                                                            </ul>
                        </nav>
                        					</li>
													 					 					 												<li class="nav-headerv5 no-link" data-action="pulldown-item" data-height="90">
						<a  href="javascript:void(0);"  						 title="Les offres" class="majuscules omniture sans-over" >
							<h4 class="">
								<span>
									Les offres
								</span>
							</h4>
						</a>
                                                <nav class="nav-4items mono">
                            <ul>
                                                                <li data-color="#0083c5" class="simple"><a title="Pack Panorama" href="/pid2270-comprendre-pack-panorama.html" >Pack Panorama</a></li>
                                                                <li data-color="#ad086c" class="simple"><a title="Pack Grand Panorama" href="/pid2272-comprendre-le-pack-panorama-le-pack-series-cinema.html" >Pack Grand Panorama</a></li>
                                                            </ul>
                        </nav>
                        					</li>
													 					 					 												<li class="nav-headerv5 " >
						<a  href="http://www.lesoffrescanal.fr/offre-tv/chaine-canalsat/?sc_intcmp=CSAT:NAV:MENU:SABONNER"  target="_blank" 						 title="S'abonner" class="majuscules omniture sans-over" onclick="siteLienExterne('CSAT:ACCUEIL:NAV:SABONNER')">
							<h4 class="">
								<span>
									S'abonner
								</span>
							</h4>
						</a>
                        					</li>
													 									 									 									 									 									 									 								</ul>
		</nav>
	</div>
</div>
<div class="pulldown"></div>

<script type="text/javascript">
	loadHabillage();
	jQuery(document).ready(function() {
		if (abonne) {
			if (!jQuery.cookie("eureka")) {
				jQuery.getScript("/ajax/private/checkEureka.php", function() {
					if (bEligibleEureka=="1") {
						jQuery.cookie("eureka", "oui", { expires: 7300 , path: '/' });
						$('ul.globale-menu li.eureka a').html("Avec&nbsp;&nbsp;"+$('ul.globale-menu li.eureka a').html());
						$('ul.globale-menu li.li-nav a').each(function() {
							if ($(this).html()=="Eureka") {
								$(this).parent("li").find("a").attr("href", "");
							}
						});
						$('#footer-nav ul.col li a').each(function() {
							if ($(this).html()=="Eureka") {
								$(this).attr("href", "");
							}
						});
					} else {
						jQuery.cookie("eureka", "non", { expires: 7300 , path: '/' });
						$('ul.globale-menu li.eureka a').html("D&eacute;couvrez&nbsp;&nbsp;"+$('ul.globale-menu li.eureka a').html());
					}
					
				});
			} else if (abonne && jQuery.cookie("eureka")=="oui") {
				$('ul.globale-menu li.eureka a').html("Avec&nbsp;&nbsp;"+$('ul.globale-menu li.eureka a').html());
				$('ul.globale-menu li.li-nav a').each(function() {
					if ($(this).html()=="Eureka") {
						$(this).parent("li").find("a").attr("href", "");
					}
				});
				$('#footer-nav ul.col li a').each(function() {
					if ($(this).html()=="Eureka") {
						$(this).attr("href", "");
					}
				});
			} else {
				$('ul.globale-menu li.eureka a').html("D&eacute;couvrez&nbsp;&nbsp;"+$('ul.globale-menu li.eureka a').html());
			}
		} else {
			$('ul.globale-menu li.eureka a').html("D&eacute;couvrez&nbsp;&nbsp;"+$('ul.globale-menu li.eureka a').html());
		}
	});
	var visiteClick = false;
	var sURLFlashsEAD = "";
	jQuery(document).ready(function() {
		//footerNavigation(); 
		//PopinWebTv();
		//navigationAuthLiveTV();
		if (logue==1 && authentif_light != 1) {
			$('.lien-authentifie').css('display', 'block');
		} else
		if (authentif==1) {
			$('.lien-authentifie').css('display', 'none');
		} else {
			$('.lien-prospect').css('display', 'block');
		}
		$('#recherche_globale-nav').submit(function() {
			if(jQuery("#search").val().length>3 && jQuery("#search").val() != "Recherche programme") {
				jQuery(location).attr('href',"?kw=" + encodeURIComponent(jQuery("#search").val()));
			}
		});
	});
</script>

<style type="text/css">
	</style>

<div id="content"><div class="wrapper">

<!-- Zone /page/gabarit_polyvalent/modules.php-->
<section class="modules module-toutes-les-chaines">
        <header>
        <h3>Découvrez toutes les chaines des offres CANALSAT</h3>
            </header>
        <section data-thematique-defaut="9,6,23,8,10,13,5,4,17,1,18" data-offre-defaut="33490">
        <div class="liste-thematiques">
            <ul>
                                                <li data-id="9" class="actif"><span>Cinema</span></li>
                                                                <li data-id="6" class="actif"><span>Découverte</span></li>
                                                                <li data-id="23" class="actif"><span>Séries et Divertissement</span></li>
                                                                <li data-id="8" class="actif"><span>Sport</span></li>
                                                                <li data-id="10" class="actif"><span>Jeunesse</span></li>
                                                                <li data-id="13" class="actif"><span>Musique </span></li>
                                                                <li data-id="5" class="actif"><span>Styles de vie</span></li>
                                                                <li data-id="4" class="actif"><span>Information</span></li>
                                                                <li data-id="17" class="actif"><span>Adulte</span></li>
                                                                <li data-id="1" class="actif"><span>Généralistes</span></li>
                                                                <li data-id="18" class="actif"><span>Chaines étrangères</span></li>
                                                <li data-id="-1" ><span>Toutes les chaines</span></li>
            </ul>
        </div>
                <div class="liste-offres">
            <ul>
                                <li data-id="33490" class="actif "><span>Pack Panorama</span></li>
                                <li data-id="33491" class=""><span>Pack Séries cinéma</span></li>
                                <li data-id="33492" class=" last"><span>Pack Panorama + Pack Séries cinéma</span></li>
                            </ul>
        </div>
                <div class="liste-chaines">
                        <section class="thematique-detail" data-id="9" >
                <header>
                    <h4>Cinema</h4>
                </header>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=1"><img title="Ciné+ Premier" alt="Ciné+ Premier" src="http://newmedia.canal-plus.com/image/50/3/96503.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=4"><img title="Ciné+ Frisson" alt="Ciné+ Frisson" src="http://newmedia.canal-plus.com/image/59/0/96590.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=3"><img title="Ciné+ Emotion" alt="Ciné+ Emotion" src="http://newmedia.canal-plus.com/image/11/8/100118.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=2"><img title="Ciné+ Famiz" alt="Ciné+ Famiz" src="http://newmedia.canal-plus.com/image/30/7/89307.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=6"><img title="Ciné+ Club" alt="Ciné+ Club" src="http://newmedia.canal-plus.com/image/31/2/89312.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=7"><img title="Ciné+ Classic" alt="" src="http://newmedia.canal-plus.com/image/30/5/89305.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=9"><img title="TCM Cinéma" alt="TCM Cinéma" src="http://newmedia.canal-plus.com/image/66/8/100668.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=2007"><img title="Paramount Channel" alt="Paramount Channel" src="http://newmedia.canal-plus.com/image/93/0/101930.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=1568"><img title="OCS Max" alt="OCS Max" src="http://newmedia.canal-plus.com/image/26/8/98268.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=1570"><img title="OCS City" alt="OCS City" src="http://newmedia.canal-plus.com/image/50/4/102504.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=1569"><img title="OCS Choc" alt="OCS Choc" src="http://newmedia.canal-plus.com/image/27/6/98276.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=1571"><img title="OCS Geants" alt="OCS Geants" src="http://newmedia.canal-plus.com/image/27/4/98274.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=10"><img title="Polar" alt="Polar" src="http://newmedia.canal-plus.com/image/33/4/99334.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=11"><img title="Ciné FX" alt="Ciné FX" src="http://newmedia.canal-plus.com/image/05/6/103056.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=62"><img title="Action" alt="Action" src="http://newmedia.canal-plus.com/image/66/7/96667.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=321"><img title="Disney Cinémagic" alt="Disney Cinémagic" src="http://newmedia.canal-plus.com/image/22/6/94226.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492]" data-thematiques="[-1,9]">
                    <a href="/pid2220-chaine.html?chaine=322"><img title="Disney Cinémagic +1" alt="Disney Cinémagic +1" src="http://newmedia.canal-plus.com/image/57/9/94579.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                        <section class="thematique-detail" data-id="6" >
                <header>
                    <h4>Découverte</h4>
                </header>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=47"><img title="Planète +" alt="Planète + sur CANALSAT" src="http://newmedia.canal-plus.com/image/19/1/103191.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=48"><img title="Planète + Thalassa" alt="Planète + Thalassa sur CANALSAT" src="http://newmedia.canal-plus.com/image/19/0/103190.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=441"><img title="Planète + CI" alt="Planète + CI sur CANALSAT" src="http://newmedia.canal-plus.com/image/18/9/103189.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=49"><img title="Planète + A&E" alt="Planète + A&E sur CANALSAT" src="http://newmedia.canal-plus.com/image/18/8/103188.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=52"><img title="Discovery Channel" alt="Discovery Channel sur CANALSAT" src="http://newmedia.canal-plus.com/image/63/7/96637.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=1905"><img title="Discovery Science" alt="Discovery Science sur CANALSAT" src="http://newmedia.canal-plus.com/image/74/1/98741.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=53"><img title="National Geographic Channel" alt="National Geographic Channel sur CANALSAT" src="http://newmedia.canal-plus.com/image/47/1/96471.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=561"><img title="Nat Geo Wild" alt="Nat Geo Wild sur CANALSAT" src="http://newmedia.canal-plus.com/image/15/8/93158.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=50"><img title="Voyage" alt="Voyage sur CANALSAT" src="http://newmedia.canal-plus.com/image/70/9/96709.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492,33490]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=61"><img title="Ushuaia TV" alt="Ushuaia TV sur CANALSAT" src="http://newmedia.canal-plus.com/image/26/7/98267.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=51"><img title="Histoire" alt="Histoire sur CANALSAT" src="http://newmedia.canal-plus.com/image/51/0/96510.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=63"><img title="Toute L'Histoire" alt="Toute L'Histoire sur CANALSAT" src="http://newmedia.canal-plus.com/image/00/5/105005.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=1525"><img title="Animaux - chaine en option" alt="Animaux - chaine en option" src="http://newmedia.canal-plus.com/image/91/3/94913.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=81"><img title="Escales - chaine en option" alt="Escales - chaine en option" src="http://newmedia.canal-plus.com/image/64/1/96641.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=65"><img title="Encyclo - chaine en option" alt="Encyclo - chaine en option" src="http://newmedia.canal-plus.com/image/29/5/89295.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=101"><img title="Seasons - chaine en option" alt="Seasons - chaine en option" src="http://newmedia.canal-plus.com/image/63/8/96638.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=1196"><img title="8 Mont Blanc" alt="8 Mont Blanc sur CANALSAT" src="http://newmedia.canal-plus.com/image/07/7/104077.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[33490,33492]" data-thematiques="[-1,6]">
                    <a href="/pid2220-chaine.html?chaine=1827"><img title="RMC Découverte" alt="RMC Découverte sur CANALSAT" src="http://newmedia.canal-plus.com/image/84/8/98848.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                        <section class="thematique-detail" data-id="23" >
                <header>
                    <h4>Séries et Divertissement</h4>
                </header>
                                <div class="chaine-detail" data-offres="[33491,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=1865"><img title="PassSeries" alt="Pass Séries, saisons et intégralité de séries sur CANALSAT" src="http://newmedia.canal-plus.com/image/05/1/99051.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=115"><img title="Série Club" alt="Série CLub sur CANALSAT" src="http://newmedia.canal-plus.com/image/52/9/99529.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=109"><img title="Jimmy" alt="Jimmy sur CANALSAT" src="http://newmedia.canal-plus.com/image/40/9/104409.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=106"><img title="13ème Rue" alt="13ème Rue  sur CANALSAT" src="http://newmedia.canal-plus.com/image/01/9/101019.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=108"><img title="Comédie +" alt="Comédie + sur CANALSAT" src="http://newmedia.canal-plus.com/image/76/5/90765.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=1009"><img title="SyFy" alt="SyFy sur CANALSAT" src="http://newmedia.canal-plus.com/image/10/4/92104.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=111"><img title="TF6" alt="TF6 sur CANALSAT" src="http://newmedia.canal-plus.com/image/61/4/96614.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=87"><img title="MTV" alt="MTV sur CANALSAT" src="http://newmedia.canal-plus.com/image/72/2/96722.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=39"><img title="June TV" alt="June TV sur CANALSAT" src="http://newmedia.canal-plus.com/image/12/4/105124.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=1825"><img title="Non Stop People" alt="Non Stop People sur CANALSAT" src="http://newmedia.canal-plus.com/image/84/6/98846.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=130"><img title="E !" alt="E ! sur CANALSAT" src="http://newmedia.canal-plus.com/image/11/9/99119.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=83"><img title="MCM" alt="MCM sur CANALSAT" src="http://newmedia.canal-plus.com/image/67/6/96676.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=2009"><img title="J One" alt="J One sur CANALSAT" src="http://newmedia.canal-plus.com/image/93/7/101937.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=37"><img title="Game One" alt="Game One sur CANALSAT" src="http://newmedia.canal-plus.com/image/67/9/96679.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=38"><img title="Mangas" alt="Mangas sur CANALSAT" src="http://newmedia.canal-plus.com/image/02/0/87020.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=40"><img title="AB1_110X82.png" alt="" src="http://newmedia.canal-plus.com/image/05/4/92054.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=114"><img title="TvBreizh_110x82.png" alt="TV Breizh sur CANALSAT" src="http://newmedia.canal-plus.com/image/58/7/106587.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=113"><img title="Téva" alt="Téva sur CANALSAT" src="http://newmedia.canal-plus.com/image/10/0/105100.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=118"><img title="Paris Première" alt="Paris Première sur CANALSAT" src="http://newmedia.canal-plus.com/image/22/0/92220.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=122"><img title="RTL9" alt="RTL9 sur CANALSAT" src="http://newmedia.canal-plus.com/image/04/9/92049.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=124"><img title="D8" alt="D8 sur CANALSAT" src="http://newmedia.canal-plus.com/image/54/9/98549.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=119"><img title="W9" alt="W9 sur CANALSAT" src="http://newmedia.canal-plus.com/image/61/9/96619.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=117"><img title="TMC" alt="TMC sur CANALSAT" src="http://newmedia.canal-plus.com/image/61/8/96618.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=121"><img title="NT1" alt="NT1 sur CANALSAT" src="http://newmedia.canal-plus.com/image/23/7/98237.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=125"><img title="NRJ12" alt="NRJ12 sur CANALSAT" src="http://newmedia.canal-plus.com/image/04/2/87042.jpg" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=116"><img title="France4" alt="France4 sur CANALSAT" src="http://newmedia.canal-plus.com/image/62/3/96623.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=1132"><img title="D17" alt="D17 sur CANALSAT" src="http://newmedia.canal-plus.com/image/54/8/98548.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=161"><img title="France O" alt="France O sur CANALSAT" src="http://newmedia.canal-plus.com/image/62/6/96626.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=1826"><img title="Numéro 23" alt="Numéro 23 sur CANALSAT" src="http://newmedia.canal-plus.com/image/84/7/98847.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=1807"><img title="Chérie 25" alt="Chérie 25 sur CANALSAT" src="http://newmedia.canal-plus.com/image/86/8/98868.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,23]">
                    <a href="/pid2220-chaine.html?chaine=123"><img title="TV5 Monde" alt="TV5 Monde sur CANALSAT" src="http://newmedia.canal-plus.com/image/11/7/99117.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                        <section class="thematique-detail" data-id="8" >
                <header>
                    <h4>Sport</h4>
                </header>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=12"><img title="Sport +" alt="Sport +" src="http://newmedia.canal-plus.com/image/32/1/89321.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=13"><img title="Eurosport" alt="Eurosport" src="http://newmedia.canal-plus.com/image/48/0/88480.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=20"><img title="Infosport +" alt="Infosport +" src="http://newmedia.canal-plus.com/image/31/5/89315.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=1705"><img title="Sport 365" alt="Sport 365" src="http://newmedia.canal-plus.com/image/20/2/98202.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=21"><img title="Eurosport 2" alt="Eurosport 2" src="http://newmedia.canal-plus.com/image/48/3/88483.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=600"><img title="Ma Chaine Sport" alt="Ma Chaine Sport" src="http://newmedia.canal-plus.com/image/64/5/96645.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=1466"><img title="Ma Chaine Sport Extreme" alt="Ma Chaine Sport Extreme" src="http://newmedia.canal-plus.com/image/46/9/94469.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=1686"><img title="Kombat Sport" alt="Kombat Sport" src="http://newmedia.canal-plus.com/image/74/0/98740.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=14"><img title="Equidia Live" alt="Equidia Live" src="http://newmedia.canal-plus.com/image/38/7/92387.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=1325"><img title="Equidia Life" alt="Equidia Life" src="http://newmedia.canal-plus.com/image/40/2/92402.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=26"><img title="Motors TV" alt="Motors TV" src="http://newmedia.canal-plus.com/image/50/2/96502.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=23"><img title="OM TV" alt="OM TV" src="http://newmedia.canal-plus.com/image/64/7/96647.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=24"><img title="OL TV" alt="OL TV" src="http://newmedia.canal-plus.com/image/64/8/96648.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=17"><img title="ONZEO" alt="ONZEO" src="http://newmedia.canal-plus.com/image/53/1/102531.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=721"><img title="Girondons TV" alt="Girondons TV" src="http://newmedia.canal-plus.com/image/65/0/96650.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=27"><img title="Nautical Channel" alt="Nautical Channel" src="http://newmedia.canal-plus.com/image/33/1/98331.jpg" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=1645"><img title="Bein Sports 1 - chaine en option" alt="Bein Sports 1 - chaine en option" src="http://newmedia.canal-plus.com/image/24/8/104248.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=1665"><img title="Bein Sports 2 - chaine en option" alt="Bein Sports 2 - chaine en option" src="http://newmedia.canal-plus.com/image/24/9/104249.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=1605"><img title="Golf + - chaine en option" alt="Golf + - chaine en option" src="http://newmedia.canal-plus.com/image/96/7/96967.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=25"><img title="Extreme Sports Channel - chaine en option" alt="Extreme Sports Channel - chaine en option" src="http://newmedia.canal-plus.com/image/65/2/96652.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=18"><img title="AB Moteurs - chaine en option" alt="AB Moteurs - chaine en option" src="http://newmedia.canal-plus.com/image/56/5/99565.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,8]">
                    <a href="/pid2220-chaine.html?chaine=19"><img title="L'Equipe 21" alt="L'Equipe 21" src="http://newmedia.canal-plus.com/image/42/0/99420.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                        <section class="thematique-detail" data-id="10" >
                <header>
                    <h4>Jeunesse</h4>
                </header>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=44"><img title="Disney Junior" alt="Disney Junior sur CANALSAT" src="http://newmedia.canal-plus.com/image/77/1/89771.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=29"><img title="Tiji" alt="Tiji sur CANALSAT" src="http://newmedia.canal-plus.com/image/84/4/98844.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=1030"><img title="Nickelodeon Junior" alt="Nickelodeon Junior sur CANALSAT" src="http://newmedia.canal-plus.com/image/67/2/96672.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=1625"><img title="Mon Nickelodeon Junior" alt="Mon Nickelodeon Junior sur CANALSAT" src="http://newmedia.canal-plus.com/image/02/3/97023.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=35"><img title="Piwi +" alt="Piwi + sur CANALSAT" src="http://newmedia.canal-plus.com/image/36/6/89366.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=341"><img title="Boomerang" alt="Boomerang sur CANALSAT" src="http://newmedia.canal-plus.com/image/29/7/91297.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=46"><img title="Nickelodeon" alt="Nickelodeon sur CANALSAT" src="http://newmedia.canal-plus.com/image/67/0/96670.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=28"><img title="Canal J" alt="Canal J sur CANALSAT" src="http://newmedia.canal-plus.com/image/66/9/96669.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=41"><img title="110x82_DisneyChannel.png" alt="" src="http://newmedia.canal-plus.com/image/56/4/106564.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=42"><img title="110x82_DisneyChannel+1.png" alt="" src="http://newmedia.canal-plus.com/image/56/6/106566.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=30"><img title="Cartoon Network" alt="Cartoon Network sur CANALSAT" src="http://newmedia.canal-plus.com/image/68/0/82680.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=34"><img title="Disney XD" alt="Disney XD sur CANALSAT" src="http://newmedia.canal-plus.com/image/67/3/96673.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=32"><img title="TéléToon +" alt="TéléToon + sur CANALSAT" src="http://newmedia.canal-plus.com/image/32/2/89322.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=33"><img title="TéléToon +1" alt="TéléToon +1 sur CANALSAT" src="http://newmedia.canal-plus.com/image/32/3/89323.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=1328"><img title="Boing" alt="Boing sur CANALSAT" src="http://newmedia.canal-plus.com/image/40/8/92408.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,10]">
                    <a href="/pid2220-chaine.html?chaine=36"><img title="Gulli" alt="Gulli sur CANALSAT" src="http://newmedia.canal-plus.com/image/84/5/98845.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                        <section class="thematique-detail" data-id="13" >
                <header>
                    <h4>Musique </h4>
                </header>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=481"><img title="MTV Base" alt="MTV Base sur CANALSAT" src="http://newmedia.canal-plus.com/image/72/1/96721.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=88"><img title="MTV Pulse" alt="MTV Pulse sur CANALSAT" src="http://newmedia.canal-plus.com/image/72/0/96720.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=89"><img title="MTV Idol" alt="MTV Idol sur CANALSAT" src="http://newmedia.canal-plus.com/image/71/9/96719.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=1245"><img title="Trace Urban" alt="Trace Urban sur CANALSAT" src="http://newmedia.canal-plus.com/image/83/1/85831.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=361"><img title="NRJ Hits" alt="NRJ Hits sur CANALSAT" src="http://newmedia.canal-plus.com/image/67/5/96675.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=84"><img title="MCM Top" alt="MCM Top sur CANALSAT" src="http://newmedia.canal-plus.com/image/67/7/96677.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=85"><img title="MCM Pop" alt="MCM Pop sur CANALSAT" src="http://newmedia.canal-plus.com/image/67/8/96678.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=911"><img title="Brava HDTV" alt="Brava HDTV sur CANALSAT" src="http://newmedia.canal-plus.com/image/68/0/96680.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=95"><img title="MTV Hits" alt="MTV Hits sur CANALSAT" src="http://newmedia.canal-plus.com/image/71/7/96717.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=93"><img title="MTV Rocks" alt="MTV Rocks sur CANALSAT" src="http://newmedia.canal-plus.com/image/71/8/96718.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=92"><img title="M6 Music" alt="M6 Music sur CANALSAT" src="http://newmedia.canal-plus.com/image/96/6/96966.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=1765"><img title="M6 Music Player - chaine en option" alt="M6 Music Player - chaine en option" src="http://newmedia.canal-plus.com/image/30/8/106308.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=781"><img title="M6 Music Black" alt="M6 Music Black sur CANALSAT" src="http://newmedia.canal-plus.com/image/71/3/96713.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=763"><img title="M6 Music Club" alt="M6 Music Club sur CANALSAT" src="http://newmedia.canal-plus.com/image/71/4/96714.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=100"><img title="Melody - chaine en option" alt="Melody - chaine en option" src="http://newmedia.canal-plus.com/image/30/9/106309.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=98"><img title="Mezzo - chaine en option" alt="Mezzo - chaine en option" src="http://newmedia.canal-plus.com/image/61/7/93617.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=1445"><img title="Mezzo Live HD - chaine en option" alt="Mezzo Live HD - chaine en option" src="http://newmedia.canal-plus.com/image/61/9/93619.jpg" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[]" data-thematiques="[-1,13]">
                    <a href="/pid2220-chaine.html?chaine=1689"><img title="DJazz TV - chaine en option" alt="DJazz TV - chaine en option" src="http://newmedia.canal-plus.com/image/19/4/98194.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                        <section class="thematique-detail" data-id="5" >
                <header>
                    <h4>Styles de vie</h4>
                </header>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,5]">
                    <a href="/pid2220-chaine.html?chaine=127"><img title="Cuisine +" alt="Cuisine + sur CANALSAT" src="http://newmedia.canal-plus.com/image/56/5/95565.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,5]">
                    <a href="/pid2220-chaine.html?chaine=128"><img title="Maison +" alt="Maison + sur CANALSAT" src="http://newmedia.canal-plus.com/image/56/6/95566.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,5]">
                    <a href="/pid2220-chaine.html?chaine=1566"><img title="Ma Chaine Sport Bien Etre" alt="Ma Chaine Sport Bien Etre sur CANALSAT" src="http://newmedia.canal-plus.com/image/56/7/95567.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                        <section class="thematique-detail" data-id="4" >
                <header>
                    <h4>Information</h4>
                </header>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=129"><img title="La Chaine Météo" alt="La Chaine Météo sur CANALSAT" src="http://newmedia.canal-plus.com/image/62/4/96624.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492,33490]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=135"><img title="LCI" alt="LCI sur CANALSAT" src="http://newmedia.canal-plus.com/image/62/9/96629.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=134"><img title="iTELE" alt="iTELE sur CANALSAT" src="http://newmedia.canal-plus.com/image/52/9/102529.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=139"><img title="BFM TV" alt="BFM TV sur CANALSAT" src="http://newmedia.canal-plus.com/image/63/1/96631.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=136"><img title="Euronews" alt="Euronews sur CANALSAT" src="http://newmedia.canal-plus.com/image/63/2/96632.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=143"><img title="France24" alt="France24 sur CANALSAT" src="http://newmedia.canal-plus.com/image/78/2/96782.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=138"><img title="LCP - Public Sénat" alt="LCP - Public Sénat sur CANALSAT" src="http://newmedia.canal-plus.com/image/29/8/106298.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=1329"><img title="BFM BUSINESS" alt="BFM BUSINESS sur CANALSAT" src="http://newmedia.canal-plus.com/image/41/2/92412.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=137"><img title="Bloomeberg Television" alt="Bloomeberg Television sur CANALSAT" src="http://newmedia.canal-plus.com/image/63/4/96634.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=140"><img title="CNN" alt="CNN sur CANALSAT" src="http://newmedia.canal-plus.com/image/71/1/96711.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=141"><img title="BBC World News" alt="" src="http://newmedia.canal-plus.com/image/95/7/24957.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=142"><img title="CNBC" alt="CNBC sur CANALSAT" src="http://newmedia.canal-plus.com/image/63/6/96636.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=1485"><img title="i24 NEWS" alt="i24 NEWS sur CANALSAT" src="http://newmedia.canal-plus.com/image/03/4/101034.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,4]">
                    <a href="/pid2220-chaine.html?chaine=165"><img title="KTO Télévision catholique" alt="KTO Télévision catholique sur CANALSAT" src="http://newmedia.canal-plus.com/image/92/0/93920.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                        <section class="thematique-detail" data-id="17" >
                <header>
                    <h4>Adulte</h4>
                </header>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,17]">
                    <a href="/pid2220-chaine.html?chaine=1866"><img title="Pass VOD XXL Dorcel - chaine en option" alt="Pass VOD XXL Dorcel - chaine en option" src="http://newmedia.canal-plus.com/image/34/8/105348.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,17]">
                    <a href="/pid2220-chaine.html?chaine=1585"><img title="French Lover TV - chaine en option" alt="French Lover TV - chaine en option" src="http://newmedia.canal-plus.com/image/41/9/96419.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,17]">
                    <a href="/pid2220-chaine.html?chaine=1265"><img title="Libido TV - chaine en option" alt="Libido TV - chaine en option" src="http://newmedia.canal-plus.com/image/94/0/86940.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,17]">
                    <a href="/pid2220-chaine.html?chaine=761"><img title="Dorcel TV - chaine en option" alt="Dorcel TV - chaine en option" src="http://newmedia.canal-plus.com/image/69/3/96693.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,17]">
                    <a href="/pid2220-chaine.html?chaine=167"><img title="XXL - chaine en option" alt="XXL - chaine en option" src="http://newmedia.canal-plus.com/image/31/0/106310.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,17]">
                    <a href="/pid2220-chaine.html?chaine=170"><img title="Dorcel XXX - chaine en option" alt="Dorcel XXX - chaine en option" src="http://newmedia.canal-plus.com/image/71/5/104715.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,17]">
                    <a href="/pid2220-chaine.html?chaine=912"><img title="Penthouse HD - chaine en option" alt="Penthouse HD - chaine en option" src="http://newmedia.canal-plus.com/image/45/4/100454.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[]" data-thematiques="[-1,17]">
                    <a href="/pid2220-chaine.html?chaine=120"><img title="Pink TV - chaine en option" alt="Pink TV - chaine en option" src="http://newmedia.canal-plus.com/image/69/0/96690.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail last" data-offres="[]" data-thematiques="[-1,17]">
                    <a href="/pid2220-chaine.html?chaine=2011"><img title="Le Porn - chaine en option" alt="Le Porn - chaine en option" src="http://newmedia.canal-plus.com/image/08/1/102081.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                        <section class="thematique-detail" data-id="1" >
                <header>
                    <h4>Généralistes</h4>
                </header>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,1]">
                    <a href="/pid2220-chaine.html?chaine=181"><img title="TF1" alt="TF1 sur CANALSAT" src="http://newmedia.canal-plus.com/image/53/0/102530.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,1]">
                    <a href="/pid2220-chaine.html?chaine=182"><img title="France2" alt="France2 sur CANALSAT" src="http://newmedia.canal-plus.com/image/59/2/96592.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,1]">
                    <a href="/pid2220-chaine.html?chaine=183"><img title="France3" alt="France3 sur CANALSAT" src="http://newmedia.canal-plus.com/image/59/3/96593.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,1]">
                    <a href="/pid2220-chaine.html?chaine=184"><img title="France5" alt="France5 sur CANALSAT" src="http://newmedia.canal-plus.com/image/59/4/96594.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,1]">
                    <a href="/pid2220-chaine.html?chaine=185"><img title="M6" alt="M6 sur CANALSAT" src="http://newmedia.canal-plus.com/image/59/5/96595.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33492]" data-thematiques="[-1,1]">
                    <a href="/pid2220-chaine.html?chaine=186"><img title="Arte" alt="Arte sur CANALSAT" src="http://newmedia.canal-plus.com/image/52/3/102523.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                        <section class="thematique-detail" data-id="18" >
                <header>
                    <h4>Chaines étrangères</h4>
                </header>
                                <div class="chaine-detail" data-offres="[33490,33491,33492]" data-thematiques="[-1,18]">
                    <a href="/pid2220-chaine.html?chaine=187"><img title="Al Jazeera" alt="" src="http://newmedia.canal-plus.com/image/95/1/24951.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33491,33492]" data-thematiques="[-1,18]">
                    <a href="/pid2220-chaine.html?chaine=1305"><img title="France24 Arabic" alt="France24 Arabic sur CANALSAT" src="http://newmedia.canal-plus.com/image/78/4/96784.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33492,33491,33490]" data-thematiques="[-1,18]">
                    <a href="/pid2220-chaine.html?chaine=521"><img title="France24 English" alt="France24 English sur CANALSAT" src="http://newmedia.canal-plus.com/image/78/3/96783.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33491,33492]" data-thematiques="[-1,18]">
                    <a href="/pid2220-chaine.html?chaine=241"><img title="Al Jazeera english" alt="" src="http://newmedia.canal-plus.com/image/95/2/24952.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33491,33492]" data-thematiques="[-1,18]">
                    <a href="/pid2220-chaine.html?chaine=782"><img title="CCTVF" alt="CCTV sur CANALSAT" src="http://newmedia.canal-plus.com/image/71/2/86712.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33491,33492]" data-thematiques="[-1,18]">
                    <a href="/pid2220-chaine.html?chaine=1238"><img title="Arirang" alt="Arirang sur CANALSAT" src="http://newmedia.canal-plus.com/image/59/2/95592.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33491,33492]" data-thematiques="[-1,18]">
                    <a href="/pid2220-chaine.html?chaine=1152"><img title="NHK World" alt="NHK World sur CANALSAT" src="http://newmedia.canal-plus.com/image/31/1/106311.png" width="70" height="54"></a>
                </div>
                                <div class="chaine-detail" data-offres="[33490,33491,33492]" data-thematiques="[-1,18]">
                    <a href="/pid2220-chaine.html?chaine=716"><img title="Russia today" alt="" src="http://newmedia.canal-plus.com/image/08/6/25086.png" width="70" height="54"></a>
                </div>
                            </section>
            <div class="clearBoth"></div>
                    </div>
    </section>
</section>
<div class="clearBoth"></div>

</div></div>
<!-- Zone /page/commun_v5/footer.php-->
<div id="footer" class="footer-v6">
	<div id="footer-nav" style="position: relative">
		<div class="wrapper" style="position: relative">
			<div class="footer-description">
				<h2 class="logo"><a href="/" title="CANALSAT" class="no-background-css" href="/"><img src="http://newmedia.canal-plus.com/image/60/8/102608.png" title="CANALSAT" alt="CANALSAT" /></a></h2>
				<p class="desc">Les programmes des différentes chaînes du bouquet et des dossiers consacrés au cinéma, sport, musique.</p>
				<p class="lien"><a href='/pid2120-mentions-legales.html'  class="trigger-lb">Mentions légales</a></p>                <p class="lien"><a href='/pid2140-cookies.html'  class="trigger-lb">Cookies</a></p>			</div>
                                                <ul class="col">
                <li class="titre">DÉCOUVRIR</li>
                                                                <li class="item"><a title="Cinéma" href="/pid2261-decouvrir-cinema.html" >Cinéma</a></li>
                                                                                <li class="item"><a title="Séries" href="/pid2262-decouvrir-serie.html" >Séries</a></li>
                                                                                <li class="item"><a title="Jeunesse" href="/pid2260-decouvrir-jeunesse.html" >Jeunesse</a></li>
                                                                                <li class="item"><a title="Campus" href="/pid2269-decouvrir-campus.html" >Campus</a></li>
                                                                                <li class="item"><a title="Découverte" href="/pid2265-decouvrir-decouverte.html" >Découverte</a></li>
                                            </ul>
                                                <ul class="col">
                <li class="titre">LES OFFRES</li>
                                                                <li class="item"><a title="Pack Panorama" href="/pid2270-comprendre-pack-panorama.html" >Pack Panorama</a></li>
                                                                                <li class="item"><a title="Pack Grand Panorama" href="/pid2272-comprendre-le-pack-panorama-le-pack-series-cinema.html" >Pack Grand Panorama</a></li>
                                            </ul>
                                                <ul class="col">
                <li class="titre"><a title="S&#039;abonner" href="http://www.lesoffrescanal.fr/offre-tv/chaine-canalsat/?sc_intcmp=CSAT:NAV:MENU:SABONNER" >S'ABONNER</a></li>
                            </ul>
                                    		</div>
	</div>
	<div id="footer-base">
		<div class="wrapper">
            			<ul>
															<li><a onclick="siteLienExterne('CSAT:NAV:FOOTER:MYCANAL')" class="pass_link pass_auth_gene omniture" href="http://mycanal.fr?sc_intcmp=CSAT:NAV:FOOTER:MYCANAL"  target="_blank">myCANAL</a></li>
																				<li><a onclick="siteLienExterne('CSAT:NAV:FOOTER:CANALPLUS')" class="pass_link pass_auth_gene omniture" href="http://www.canalplus.fr/?sc_intcmp=CSAT:NAV:FOOTER:CANALPLUS"  target="_blank">CANAL+</a></li>
																				<li><a onclick="siteLienExterne('CSAT:NAV:FOOTER:CANALPLAY')" class="pass_link pass_auth_gene omniture" href="http://www.canalplay.com/?sc_intcmp=CSAT:NAV:FOOTER:CANALPLAY"  target="_blank">CANALPLAY</a></li>
																				<li><a onclick="siteLienExterne('CSAT:NAV:FOOTER:CPLUSGROUPE')" class="pass_link pass_auth_gene omniture" href="http://www.canalplusgroupe.com?sc_intcmp=CSAT:NAV:FOOTER:CPLUSGROUPE"  target="_blank">GROUPE CANAL+</a></li>
																				<li><a onclick="siteLienExterne('CSAT:NAV:FOOTER:CANALSATDLM')" class="pass_link pass_auth_gene omniture" href="http://www.canalplus-overseas.com/?sc_intcmp=CSAT:NAV:FOOTER:CANALSATDLM"  target="_blank">CANALSAT DANS LE MONDE</a></li>
												</ul>
                        <p>© CANALSAT 2014</p>
            		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	footerNavigation();
	navigationAuth();
	navigationAuthLiveTV();
	PopinWebTv();
	fixCarrousel();
	$('#footer-nav li.dynamic').each(function() {
		$(this).mouseout(function() {
			img = $(this).attr('data-img-out');
			if(img != '') {
				$(this).css("background-image", "url('"+img+"')");
			}
		});
		$(this).mouseover(function() {
			img = $(this).attr('data-img-hover');
			if(img != '') {
				$(this).css("background-image", "url('"+img+"')");
			}
		});
	});
	$('#footer-nav li.dynamic').each(function() {
		$(this).bind('click', function(e) {
			e.preventDefault();
			_href = $(this).find('a').attr('href');
			_target = $(this).find('a').attr('target');
			if (_target=='_blank') {
				window.open(_href);
			}
			else {
				document.location.href = _href;
			}
		});
	});
	$('#home-left .item, #home-right .item').each(function() {
		if (!$(this).find('a').hasClass('lien-popin-fiche-parole')) {
			$(this).bind('click', function(e) {
				e.preventDefault();
				_href = $(this).find('a').attr('href');
				_target = $(this).find('a').attr('target');
				if (_target=='_blank') {
					window.open(_href);
				}
				else {
					document.location.href = _href;
				}
			});
		}
	});
});
</script>
</div>

<!--MODAL INSCRIPTION-->
<div id="inscription"></div>
<a href="/ajax/private/signon.php?_step_=inscription_form&from=http%3A%2F%2Fwww.canalsat.fr%2Fpid2280-toutes-les-chaines.html%3Fpid%3D2280" class="lien-popin-inscription" style="display:none;"></a>
<!--MODAL IDENTIFICATION-->
<div id="identification"></div>
<div id="identification-webtv"></div>
<div id="controle-acces-webtv"></div>
<a href="/ajax/private/signon.php?_step_=identification_form&from=http%3A%2F%2Fwww.canalsat.fr%2Fpid2280-toutes-les-chaines.html%3Fpid%3D2280" class="lien-popin-identification" style="display:none;"></a>
<a href="/ajax/private/signon.php?from=http%3A%2F%2Fwww.canalsat.fr%2Fpid2280-toutes-les-chaines.html%3Fpid%3D2280" class="lien-popin-identification-footplus" style="display:none;"></a>
<!--MODAL PREMIERE VISITE-->
<div id="premierevisite"></div>
<a href="/ajax/private/signon.php?_step_=premierevisite_form&from=http%3A%2F%2Fwww.canalsat.fr%2Fpid2280-toutes-les-chaines.html%3Fpid%3D2280" class="lien-popin-premierevisite" style="display:none;"></a>
<!--MODAL MOT DE PASSE OUBLIE-->
<div id="mdp-oublie"></div>
<a href="/ajax/private/signon.php?_step_=password_oublie_form&from=http%3A%2F%2Fwww.canalsat.fr%2Fpid2280-toutes-les-chaines.html%3Fpid%3D2280" class="lien-popin-mdp-oublie" style="display:none;"></a>
<!--MODAL CONFIRMATION INSCRIPTION-->
<div id="inscription-validee"></div>
<a href="/ajax/private/signon.php?_step_=inscription_confirm&from=http%3A%2F%2Fwww.canalsat.fr%2Fpid2280-toutes-les-chaines.html%3Fpid%3D2280" class="lien-popin-inscription-confirm" style="display:none;"></a>
<!--MODAL CONFIRMATION MOT DE PASSE OUBLIE-->
<div id="mdp-oublie-validee"></div>
<a href="/ajax/private/signon.php?_step_=password_oublie_confirm&from=http%3A%2F%2Fwww.canalsat.fr%2Fpid2280-toutes-les-chaines.html%3Fpid%3D2280" class="lien-popin-mdp-confirm" style="display:none;"></a>
<!--MODAL ALERTE FOOTPLUS-->
<div id="alerte-footplus"></div>
<a href="/alerte_footplus.html" class="lien-popin-alerte-footplus" style="display:none;"></a>
<!--MODAL POPIN PRESENTATION WEB TV-->
<div id="popin-presentation-webtv"></div>
<div id="layer-info-eureka"></div>
<script type="text/javascript">
$(document).ready(function() {
	//chargement des fonctions inscription
		
		PopinWebTv();
		inscriptionFunctions();
	//identification header
	waitForPassJSON('loadHeaderAuthGene');
	//identificationHeader('2280', 'http%3A%2F%2Fwww.canalsat.fr%2Fpid2280-toutes-les-chaines.html%3Fpid%3D2280', '');
													});
function loadHeaderAuthGene() {
	loadHeaderAuth('2280', 'http%3A%2F%2Fwww.canalsat.fr%2Fpid2280-toutes-les-chaines.html%3Fpid%3D2280', '');
}
</script><!-- SiteCatalyst code version: H.11.
Copyright 1997-2007 Omniture, Inc. More info available at
http://www.omniture.com -->
<script language="JavaScript" src="//www.canalsat.fr/lib/front_tools/js/s_code_csat2.php?account=cplusglobalprod,cpluscsatprod&js=javascript:,canal-plus.com,canalsat.fr"></script>
<script language="JavaScript"><!--
/* You may give each page an identifying name, server, and channel on
the next lines. */
s.pageName="CSAT - Decouvrir - Chaines"
s.channel="CANALSAT.FR"
s.pageType=""
s.dc=""
s.visitorNamespace=""
s.cookieDomainPeriods=""
s.currencyCode=""
s.prop1=""
s.prop2=""
s.prop4="CSAT - Decouvrir"
s.prop5="CSAT - Decouvrir - Chaines"
s.prop6=""
s.prop7=""
s.prop9=""
s.prop10=""
s.prop11=""
s.prop12=""
s.prop13=""
s.prop14=""
s.prop15=""
s.prop16=""
s.prop17=""
s.prop18=""
s.prop28="Aucune"
/* E-commerce Variables */
s.campaign=""
s.state=""
s.zip=""
s.events=""
s.products=""
s.purchaseID=""
s.eVar1=""
s.eVar2=""
s.eVar3=""
s.eVar4=""
s.eVar5=""
s.eVar6=""
s.eVar7=""
s.eVar8=""
s.eVar9=""
s.eVar10=""
s.eVar11=""
s.eVar12=""
s.eVar13="non identifie"
s.eVar14="inconnu"
s.eVar15=""
s.eVar16=""
s.eVar17=""
s.eVar21=""
s.eVar22=""
s.eVar24=""
s.eVar25=""
s.eVar26=""
s.eVar27=""
s.eVar28=""
s.eVar29=""
s.eVar31=""
s.eVar36=""
s.eVar37=""
s.eVar38=""
s.eVar39=""
s.eVar40=""
s.eVar49=""
s.eVar50=""
/* Hierarchy Variables */
s.hier1=""

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
//--></script>
<script language="JavaScript"><!--
if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
//--></script><noscript><a href="http://www.omniture.com" title="Web Analytics"><img
src="http://canalplus.112.2o7.net/b/ss/cplusglobalprod,cpluscsatprod/1/H.11--NS/0"
height="1" width="1" border="0" alt="" /></a></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.11. -->

<div id="fiche"></div>
<div id="lb-mask"></div>
<div id="lb-mask-load"></div>
<div id="lb-mask-chaine"></div>
<div id="lb-mask-load-chaine"></div>  
<div id="container-loader"></div>
<div class="lb lb-alt">
	<div id="lb">
		<div class="wrap">
			<div id="detailprogram-inner"></div>
			<p class="close"><a href="#" id="lb-close-alt">Fermer</a></p>
		</div>
	</div>
</div>	
<div id="envoyer-ami"></div>
<div id="layer-alerte"></div>
<div id="layer-video"></div>
<div id="layer-ba-video"></div>
<a href="lien" id="lien_auto" class="lien" style="display:none">link</a>

<script type="text/javascript">
	$(document).ready(function() {
					});
</script>
<script src="http://media.canal-plus.com/cnil/cnil.js"></script><script type="text/javascript">_satellite.pageBottom();</script>
</body>
</html>

