#!/usr/bin/perl

use warnings;
use strict;
use LWP;
use LWP::Simple;

# configurer le fichier de sauvegarde
	my $home = $ENV{HOME};
	$home = '.' if not defined $home;
	my $conf_dir = "$home/.mythtv";
	(-d $conf_dir) or mkdir($conf_dir, 0777)
	or die "cannot mkdir $conf_dir: $!";
	my $config_file ="$conf_dir/bouquets_canalsat";
	
	
	
# ouvrir le fichier de configuration
	open(CONF, "> $config_file") or die "Cannot write to $config_file: $!";
 


# recuperation des chaines des packs de canalsat

	my $url = ("http://www.lesoffrescanal.fr/rub-canalsat/canalsat-offres-et-tarifs/formules-et-tarifs/les-packs-canalsat");
	my $content = get("$url");     	# mettre le contenu dans une variable

	my @content = split (/div id/, $content);
	foreach my $split (@content){
		(my $source )=($split =~m{=\"(.*?)" class=\"channels}); 
		my @package = split(/<dt class=\"handle\">/,$split);                                                         # copier en lignes
		foreach my $pack ( @package )
		{

			(my $option) = ($pack =~m{(.*?)<span class="channels-count">});            # extraire le nom
			$option=~ s/\s+$// if $option;
			$option=~ s/^\s+// if $option;
			my @list_chaine = split(/<span class=\"sub-4-cols\">/,$pack);  

			
			foreach my $ligne ( @list_chaine )
				{	
				(my $name) = ($ligne =~m{g\" alt=\"(.*?)\" />});            # extraire le nom
				$name=~ s/^EC - // if $name;
				my @i =  ( 'SANS PRIX', ' TV', ' +', 'EC - ', ' ', '\'', '&amp;');
				for my $i (@i)
					{
						$name =~ s/$i// if $name;                                                                            # supprimer les termes definis dans @i
					}
				$name = uc $name if $name;

#				print "$source|$option|$name|\n"if $name and $option;
				print CONF "$source|$option|$name|\n" if $name and $option; ;
				}
			}
		}


#recuperation des chaines de l'option thematiques
	$url = ('http://www.canalsat.fr/pid1011-toutes-les-chaines.html');
	$content = get("$url");     	# mettre le contenu dans une variable

	@content = split (/div id="thema_pano/, $content);
#	foreach my $split (@content){
#		(my $source )=($split =~m{=\"(.*?)" class=\"chaine"}); 
#		my @package = split(/<div class=\"chaine\">/,$split);                                                         # copier en lignes
		foreach my $pack ( @content ){
#print $pack,"\n";
			(my $option) = ($pack =~m{a title=\"(.*?)\" class});            # extraire le nom
			$option=~ s/\s+$// if $option;
			$option=~ s/^\s+// if $option;
			$option = "Thematiques \- $option" if $option;
			my @list_chaine = split(/<div class=\"chaine\">/,$pack);  
			foreach my $ligne ( @list_chaine ){	
#print $ligne,"\n\n\n\n";
				(my $name) = ($ligne =~m{title=\"(.*?)\"\>canal});           # extraire le nom
				$name=~ s/^EC - // if $name;
				my @i =  ( 'SANS PRIX', ' +', 'EC - ', ' ', '\-', '&amp;', 'Universal', ' ');
				for my $i (@i)
					{
						$name =~ s/$i// if $name;                                                                            # supprimer les termes definis dans @i
					}
				$name = uc $name if $name;

#				print "$source|$option|$name|\n"if $name and $option;
				print CONF "|$option|$name|\n" if $name and $option; ;
			}
		}
	
	
print "Fichier sauvegarde sous ~/.mythtv/bouquets_canalsat";