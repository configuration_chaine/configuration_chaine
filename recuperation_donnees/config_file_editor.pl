#!/usr/bin/perl
use strict;
use warnings;
use Storable;
use Storable qw(nstore retrieve); 

my $home = $ENV{HOME};
	$home = '.' if not defined $home;
my $conf_dir = "$home/.mythtv"; 
my $config_file ="$conf_dir/config_chaine";

#print "$config_file \n";

my %data= %{retrieve($config_file) };



#modifier la base de données (20 champs)
	for my $nom_source (sort keys %data) {
		my @list_bouquet = sort keys %{$data{$nom_source}}; 
		for my $nom_bouquet (@list_bouquet) {
			my @list_options = sort keys %{$data{$nom_source}{$nom_bouquet}};	
			for my $nom_option (@list_options) {
				my @list_chaines = sort keys %{$data{$nom_source}{$nom_bouquet}{$nom_option}};
				for my $nom_chaine (@list_chaines) {
					foreach my $chaine (qw(BFMTV BFMBUSINESS BLOOMBERGTV SPORT TV5MONDE 21 TELE5)){
						delete ($data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine})if $nom_source eq 'supprimer';
						
					}
									
					# mettre les chaines dans la table de hash en majuscule sans espace	
						my $nom_chaine_new = $nom_chaine ;
						$nom_chaine_new =~ s/\s+//g ;	
#						print "$nom_chaine_new , $nomchaine\n";									# supprime les espaces
					if ($nom_chaine ne $nom_chaine_new) {
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{name} = $nom_chaine_new;
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{option} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{option};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{bouquet} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{bouquet};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{source} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{source};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{numero_tnt} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_tnt};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{numero_tnt_hd} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_tnt_hd};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{numero_canalsat_hd} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_canalsat_hd};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{numero_canalsat_new} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_canalsat_new};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{numero_canalsat} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_canalsat};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{numero_canalsat_hd_new} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_canalsat_hd_new};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{xmltvid_kazer} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{xmltvid_kazer};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{xmltvid_mc2xml} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{xmltvid_mc2xml};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{xmltvid_telepoche} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{xmltvid_telepoche};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{xmltvid_telerama} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{xmltvid_telerama};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{xmltvid_iphone} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{xmltvid_iphone};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{icone} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{icone};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{sid} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{sid};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{cocher} = '0';
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{clean_name} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{clean_name};
						$data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine_new}{source_name} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{source_name};
						delete ($data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine});
					}else{}					
					
					if ($nom_bouquet ne lc($nom_bouquet)){
						my $nom_bouquet_new = lc($nom_bouquet);
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{name} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{name};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{option} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{option};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{bouquet} = $nom_bouquet_new;
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{source} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{source};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{numero_tnt} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_tnt};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{numero_tnt_hd} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_tnt_hd};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{numero_canalsat} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_canalsat};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{numero_canalsat_hd} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_canalsat_hd};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{numero_canalsat_new} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_canalsat_new};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{numero_canalsat_hd_new} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{numero_canalsat_hd_new};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{xmltvid_kazer} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{xmltvid_kazer};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{xmltvid_mc2xml} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{xmltvid_mc2xml};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{xmltvid_telepoche} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{xmltvid_telepoche};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{xmltvid_telerama} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{xmltvid_telerama};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{xmltvid_iphone} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{xmltvid_iphone};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{icone} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{icone};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{sid} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{sid};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{cocher} = '0';
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{clean_name} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{clean_name};
						$data{$nom_source}{$nom_bouquet_new}{$nom_option}{$nom_chaine}{source_name} = $data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine}{source_name};
						delete ($data{$nom_source}{$nom_bouquet}{$nom_option}{$nom_chaine});
#						delete ($data{$nom_source}{$nom_bouquet}{$nom_option});
#						delete ($data{$nom_source}{$nom_bouquet});
						
					}else{}
					

#					
				}}
				delete ($data{$nom_source}{$nom_bouquet}{'freq_alternative'});
				}
				delete ($data{$nom_source}{'Astra-free'});
				delete ($data{'supprimer'});
				delete ($data{$nom_source}{'hors bouquet'});
				delete ($data{$nom_source}{'hors_bouquet'});
				delete ($data{$nom_source}{'horsbouquet'});
				
			}
#lancer la sauvegarde
	nstore ( \%data, $config_file)|| die "can't store to $config_file\n";				
				
				
#imprimer la base
use Data::Dump qw(pp); # require installation
use Data::Dumper;         # require installation
	# imprimer la table de hash		
		my $hrefdata = \%data;
#		print "printing apres recharge\n";
		print pp $hrefdata;
		print "\n";
		print "%data \n";
